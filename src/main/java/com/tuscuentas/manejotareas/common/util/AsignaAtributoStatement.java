/*
 * ContextDataResourceNames.java
 *
 * Proyecto: cnk
 * Cliente: cnk
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */

package com.tuscuentas.manejotareas.common.util;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Calendar;

/**
 *
 * @author carlos
 */
public class AsignaAtributoStatement {

    /**
     *
     * @param i
     * @param contadorPrincipal
     * @param ps
     */
    public static void setInt(int i, int contadorPrincipal, PreparedStatement ps) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param i
     * @param contadorPrincipal
     * @param ps
     */
    public static void setString(int i, int contadorPrincipal, PreparedStatement ps) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     *
     */
    public AsignaAtributoStatement() {
    }

    /**
     *
     * @param index
     * @param valor
     * @param p
     * @throws SQLException
     */
    public static void setString(int index, String valor, PreparedStatement p) throws SQLException {
        if (ValidaString.isNullOrEmptyString(valor)) {
            p.setNull(index, java.sql.Types.CHAR);
        } else {
            p.setString(index, valor.trim());
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param p
     * @throws SQLException
     */
    public static void setInt(int index, String valor, PreparedStatement p) throws SQLException {
        if (ValidaString.isNullOrEmptyString(valor)) {
            p.setNull(index, java.sql.Types.INTEGER);
        } else {
            p.setInt(index, Integer.parseInt(valor.trim()));
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param p
     * @throws SQLException
     */
    public static void setFloat(int index, String valor, PreparedStatement p) throws SQLException {
        if (ValidaString.isNullOrEmptyString(valor)) {
            p.setNull(index, java.sql.Types.FLOAT);
        } else {
            p.setFloat(index, Float.parseFloat(valor.trim()));
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param ps
     * @throws SQLException
     */
    public static void setDouble(int index, String valor, PreparedStatement ps) throws SQLException {
        if (ValidaString.isNullOrEmptyString(valor)) {
            ps.setNull(index, java.sql.Types.DOUBLE);
        } else {
            ps.setDouble(index, Double.parseDouble(valor.trim()));
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param c
     * @throws SQLException
     */
    public static void setString(int index, String valor, CallableStatement c) throws SQLException {
        if (ValidaString.isNullOrEmptyString(valor)) {
            c.setNull(index, java.sql.Types.CHAR);
        } else {
            c.setString(index, valor.trim());
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param c
     * @throws SQLException
     */
    public static void setInt(int index, String valor, CallableStatement c) throws SQLException {

        if (ValidaString.isNullOrEmptyString(valor)) {
            c.setNull(index, java.sql.Types.INTEGER);
        } else {
            c.setInt(index, Integer.parseInt(valor.trim()));
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param c
     * @throws SQLException
     */
    public static void setLong(int index, String valor, CallableStatement c) throws SQLException {
        if (ValidaString.isNullOrEmptyString(valor)) {
            c.setNull(index, java.sql.Types.INTEGER);
        } else {
            c.setLong(index, Long.parseLong(valor.trim()));
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param ps
     * @throws SQLException
     */
    public static void setLong(int index, String valor, PreparedStatement ps) throws SQLException {
        if (ValidaString.isNullOrEmptyString(valor)) {
            ps.setNull(index, java.sql.Types.INTEGER);
        } else {
            ps.setLong(index, Long.parseLong(valor.trim()));
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param c
     * @throws SQLException
     */
    public static void setFloat(int index, String valor, CallableStatement c) throws SQLException {
        if (ValidaString.isNullOrEmptyString(valor)) {
            c.setNull(index, java.sql.Types.FLOAT);
        } else {
            c.setFloat(index, Float.parseFloat(valor.trim()));
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param c
     * @throws SQLException
     */
    public static void setDate(int index, String valor, CallableStatement c) throws SQLException {
        if (ValidaString.isNullOrEmptyString(valor)) {
            c.setNull(index, java.sql.Types.DATE);
        } else {
            c.setDate(index, Formato.parseSqlDate(valor, Generales.DATE_FECHA));
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param ps
     * @throws SQLException
     */
    public static void setDate(int index, String valor, PreparedStatement ps) throws SQLException {
        if (ValidaString.isNullOrEmptyString(valor)) {
            ps.setNull(index, java.sql.Types.DATE);
        } else {
            ps.setDate(index, Formato.parseSqlDate(valor,
                    Generales.DATE_FECHA_HORA));
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param ps
     * @throws SQLException
     */
    public static void setDate(int index, java.sql.Date valor, PreparedStatement ps) throws SQLException {
        if (valor == null) {
            ps.setNull(index, java.sql.Types.DATE);
        } else {
            ps.setDate(index, valor);
        }
    }

    /**
     *
     * @param index
     * @param cal
     * @param ps
     * @throws SQLException
     */
    public static void setDate(int index, Calendar cal, PreparedStatement ps) throws SQLException {
        if (cal == null) {
            ps.setNull(index, java.sql.Types.DATE);
        } else {
            ps.setObject(index, Formato.parseSqlTimestamp(cal));
        }
    }

    /**
     *
     * @param index
     * @param cal
     * @param cs
     * @throws SQLException
     */
    public static void setDate(int index, Calendar cal, CallableStatement cs) throws SQLException {
        if (cal == null) {
            cs.setNull(index, java.sql.Types.DATE);
        } else {
            cs.setObject(index, Formato.parseSqlDate(cal));
        }
    }

    /**
     *
     * @param index
     * @param valor
     * @param ps
     * @throws SQLException
     */
    public static void setTime(int index, Time valor, PreparedStatement ps) throws SQLException {
        if (valor == null) {
            ps.setNull(index, java.sql.Types.DATE);
        } else {
            ps.setTime(index, valor);
        }
    }
    
}
