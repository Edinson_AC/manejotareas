/*
 * ContextDataResourceNames.java
 *
 * Proyecto: cnk
 * Cliente: cnk
 * Copyright 2017 by Mobiltech SAS 
 * All rights reserved
 */

package com.tuscuentas.manejotareas.common.util;

/**
 *
 * @author carlos
 */
public class ValidaString {
 
    /**
     *
     */
    public ValidaString() {
    }

    /**
     *
     * @param str
     * @return
     */
    public static boolean isEmptyString(String str) {
        if (str.length() == 0) {
            return true;
        }
        if (str.equals(Generales.EMPTYSTRING)) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param str
     * @return
     */
    public static boolean isNullString(String str) {
        if (str == null) {
            return true;
        }
        if (str.equals(Generales.NULLVALUE)) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param str
     * @return
     */
    public static boolean isNullOrEmptyString(String str) {
        if (ValidaString.isNullString(str)) {
            return true;
        }
        if (ValidaString.isEmptyString(str)) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param str
     * @return
     */
    public static boolean isNumeric(String str){
	try {
		Integer.parseInt(str);
		return true;
	} catch (NumberFormatException e){
		return false;
	}
}
    
}
