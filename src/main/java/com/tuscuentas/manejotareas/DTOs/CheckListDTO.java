/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;

/**
 *
 * @author DIEGO
 */
public class CheckListDTO {

    String idCheck = Generales.EMPTYSTRING;
    String orden = Generales.EMPTYSTRING;
    String descripcion = Generales.EMPTYSTRING;
    String idActividad = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
    String ayuda = Generales.EMPTYSTRING;
    String idTareaPreActividadPre = Generales.EMPTYSTRING;
    String idTareaEmpresa = Generales.EMPTYSTRING;
    String idUsuario = Generales.EMPTYSTRING;

    /**
     *
     * @return
     */
    public String getIdCheck() {
        return idCheck;
    }

    /**
     *
     * @param idCheck
     */
    public void setIdCheck(String idCheck) {
        this.idCheck = idCheck;
    }

    /**
     *
     * @return
     */
    public String getOrden() {
        return orden;
    }

    /**
     *
     * @param orden
     */
    public void setOrden(String orden) {
        this.orden = orden;
    }

    /**
     *
     * @return
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     *
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     *
     * @return
     */
    public String getIdActividad() {
        return idActividad;
    }

    /**
     *
     * @param idActividad
     */
    public void setIdActividad(String idActividad) {
        this.idActividad = idActividad;
    }

    /**
     *
     * @return
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * @return
     */
    public String getAyuda() {
        return ayuda;
    }

    /**
     *
     * @param ayuda
     */
    public void setAyuda(String ayuda) {
        this.ayuda = ayuda;
    }

    /**
     *
     * @return
     */
    public String getIdTareaPreActividadPre() {
        return idTareaPreActividadPre;
    }

    /**
     *
     * @param idTareaPreActividadPre
     */
    public void setIdTareaPreActividadPre(String idTareaPreActividadPre) {
        this.idTareaPreActividadPre = idTareaPreActividadPre;
    }

    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

    public String getIdTareaEmpresa() {
        return idTareaEmpresa;
    }

    public void setIdTareaEmpresa(String idTareaEmpresa) {
        this.idTareaEmpresa = idTareaEmpresa;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

}
