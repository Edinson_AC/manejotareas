/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;
import java.io.Serializable;

/**
 *
 * @author Administrador
 */
public class ActividadPreDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String descripcion = Generales.EMPTYSTRING;
    String orden = Generales.EMPTYSTRING;
    String registradoPor = Generales.EMPTYSTRING;
    String fechaRegistro = Generales.EMPTYSTRING;
    String idUsuario = Generales.EMPTYSTRING;

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     *
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     *
     * @return
     */
    public String getOrden() {
        return orden;
    }

    /**
     *
     * @param orden
     */
    public void setOrden(String orden) {
        this.orden = orden;
    }

    /**
     *
     * @return
     */
    public String getRegistradoPor() {
        return registradoPor;
    }

    /**
     *
     * @param registradoPor
     */
    public void setRegistradoPor(String registradoPor) {
        this.registradoPor = registradoPor;
    }

    /**
     *
     * @return
     */
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     *
     * @param fechaRegistro
     */
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    /**
     *
     * @return
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
