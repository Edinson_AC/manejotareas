/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;

/**
 *
 * @author DIEGO
 */
public class LogActividadesDTO {

    String idLogActividad = Generales.EMPTYSTRING;
    String idActividad = Generales.EMPTYSTRING;
    String idUsuarioDevuelve = Generales.EMPTYSTRING;
    String idUsuarioDevolvieron = Generales.EMPTYSTRING;
    String observacion = Generales.EMPTYSTRING;
    String idTipoLog = Generales.EMPTYSTRING;
    String usuario = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
    String estadoProrroga = Generales.EMPTYSTRING;
    String idEjecucion = Generales.EMPTYSTRING;
    String nombreTarea = Generales.EMPTYSTRING;
    String nombreActividad = Generales.EMPTYSTRING;
    String estadoLeido = Generales.EMPTYSTRING;
    String alertasNuevas = Generales.EMPTYSTRING;
    String idEmpresa = Generales.EMPTYSTRING;
    String nombreEmpresa = Generales.EMPTYSTRING;
    String nombreResponsable = Generales.EMPTYSTRING;
    String registradoPor = Generales.EMPTYSTRING;
    String estadoObservacion = Generales.EMPTYSTRING;
    String dias = Generales.EMPTYSTRING;
    String idUsuarioAsigna = Generales.EMPTYSTRING;
    String idUsuarioAsignado = Generales.EMPTYSTRING;
    String fechaFinal = Generales.EMPTYSTRING;
    String idContrato = Generales.EMPTYSTRING;
    String alertaTemprana = Generales.EMPTYSTRING;
    String fechaRegistro = Generales.EMPTYSTRING;
    String copy = Generales.EMPTYSTRING;
    String titulo = Generales.EMPTYSTRING;
    String nombre = Generales.EMPTYSTRING;
    String correo = Generales.EMPTYSTRING;
    String plataforma = Generales.EMPTYSTRING;
    String idComentario = Generales.EMPTYSTRING;

    /**
     *
     * @return
     */
    public String getIdLogActividad() {
        return idLogActividad;
    }

    /**
     *
     * @param idLogActividad
     */
    public void setIdLogActividad(String idLogActividad) {
        this.idLogActividad = idLogActividad;
    }

    /**
     *
     * @return
     */
    public String getIdActividad() {
        return idActividad;
    }

    /**
     *
     * @param idActividad
     */
    public void setIdActividad(String idActividad) {
        this.idActividad = idActividad;
    }

    /**
     *
     * @return
     */
    public String getIdUsuarioDevuelve() {
        return idUsuarioDevuelve;
    }

    /**
     *
     * @param idUsuarioDevuelve
     */
    public void setIdUsuarioDevuelve(String idUsuarioDevuelve) {
        this.idUsuarioDevuelve = idUsuarioDevuelve;
    }

    /**
     *
     * @return
     */
    public String getIdUsuarioDevolvieron() {
        return idUsuarioDevolvieron;
    }

    /**
     *
     * @param idUsuarioDevolvieron
     */
    public void setIdUsuarioDevolvieron(String idUsuarioDevolvieron) {
        this.idUsuarioDevolvieron = idUsuarioDevolvieron;
    }

    /**
     *
     * @return
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     *
     * @param observacion
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     *
     * @return
     */
    public String getIdTipoLog() {
        return idTipoLog;
    }

    /**
     *
     * @param idTipoLog
     */
    public void setIdTipoLog(String idTipoLog) {
        this.idTipoLog = idTipoLog;
    }

    /**
     *
     * @return
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     *
     * @param usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     *
     * @return
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * @return
     */
    public String getEstadoProrroga() {
        return estadoProrroga;
    }

    /**
     *
     * @param estadoProrroga
     */
    public void setEstadoProrroga(String estadoProrroga) {
        this.estadoProrroga = estadoProrroga;
    }

    /**
     *
     * @return
     */
    public String getIdEjecucion() {
        return idEjecucion;
    }

    /**
     *
     * @param idEjecucion
     */
    public void setIdEjecucion(String idEjecucion) {
        this.idEjecucion = idEjecucion;
    }

    /**
     *
     * @return
     */
    public String getNombreTarea() {
        return nombreTarea;
    }

    /**
     *
     * @param nombreTarea
     */
    public void setNombreTarea(String nombreTarea) {
        this.nombreTarea = nombreTarea;
    }

    /**
     *
     * @return
     */
    public String getNombreActividad() {
        return nombreActividad;
    }

    /**
     *
     * @param nombreActividad
     */
    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    /**
     *
     * @return
     */
    public String getEstadoLeido() {
        return estadoLeido;
    }

    /**
     *
     * @param estadoLeido
     */
    public void setEstadoLeido(String estadoLeido) {
        this.estadoLeido = estadoLeido;
    }

    /**
     *
     * @return
     */
    public String getAlertasNuevas() {
        return alertasNuevas;
    }

    /**
     *
     * @param alertasNuevas
     */
    public void setAlertasNuevas(String alertasNuevas) {
        this.alertasNuevas = alertasNuevas;
    }

    /**
     *
     * @return
     */
    public String getIdEmpresa() {
        return idEmpresa;
    }

    /**
     *
     * @param idEmpresa
     */
    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     *
     * @return
     */
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    /**
     *
     * @param nombreEmpresa
     */
    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    /**
     *
     * @return
     */
    public String getNombreResponsable() {
        return nombreResponsable;
    }

    /**
     *
     * @param nombreResponsable
     */
    public void setNombreResponsable(String nombreResponsable) {
        this.nombreResponsable = nombreResponsable;
    }

    /**
     *
     * @return
     */
    public String getRegistradoPor() {
        return registradoPor;
    }

    /**
     *
     * @param registradoPor
     */
    public void setRegistradoPor(String registradoPor) {
        this.registradoPor = registradoPor;
    }

    /**
     *
     * @return
     */
    public String getEstadoObservacion() {
        return estadoObservacion;
    }

    /**
     *
     * @param estadoObservacion
     */
    public void setEstadoObservacion(String estadoObservacion) {
        this.estadoObservacion = estadoObservacion;
    }

    /**
     *
     * @return
     */
    public String getDias() {
        return dias;
    }

    /**
     *
     * @param dias
     */
    public void setDias(String dias) {
        this.dias = dias;
    }

    /**
     *
     * @return
     */
    public String getIdUsuarioAsigna() {
        return idUsuarioAsigna;
    }

    /**
     *
     * @param idUsuarioAsigna
     */
    public void setIdUsuarioAsigna(String idUsuarioAsigna) {
        this.idUsuarioAsigna = idUsuarioAsigna;
    }

    /**
     *
     * @return
     */
    public String getIdUsuarioAsignado() {
        return idUsuarioAsignado;
    }

    /**
     *
     * @param idUsuarioAsignado
     */
    public void setIdUsuarioAsignado(String idUsuarioAsignado) {
        this.idUsuarioAsignado = idUsuarioAsignado;
    }

    /**
     *
     * @return
     */
    public String getFechaFinal() {
        return fechaFinal;
    }

    /**
     *
     * @param fechaFinal
     */
    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    /**
     *
     * @return
     */
    public String getIdContrato() {
        return idContrato;
    }

    /**
     *
     * @param idContrato
     */
    public void setIdContrato(String idContrato) {
        this.idContrato = idContrato;
    }

    /**
     *
     * @return
     */
    public String getAlertaTemprana() {
        return alertaTemprana;
    }

    /**
     *
     * @param alertaTemprana
     */
    public void setAlertaTemprana(String alertaTemprana) {
        this.alertaTemprana = alertaTemprana;
    }

    /**
     *
     * @return
     */
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     *
     * @param fechaRegistro
     */
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    /**
     *
     * @return
     */
    public String getCopy() {
        return copy;
    }

    /**
     *
     * @param copy
     */
    public void setCopy(String copy) {
        this.copy = copy;
    }

    /**
     *
     * @return
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     *
     * @param titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public String getCorreo() {
        return correo;
    }

    /**
     *
     * @param correo
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     *
     * @return
     */
    public String getPlataforma() {
        return plataforma;
    }

    /**
     *
     * @param plataforma
     */
    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    /**
     *
     * @return
     */
    public String getIdComentario() {
        return idComentario;
    }

    /**
     *
     * @param idComentario
     */
    public void setIdComentario(String idComentario) {
        this.idComentario = idComentario;
    }
    
    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
