/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class DatosUsuarioDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String idUsuario = Generales.EMPTYSTRING;
    String usuario = Generales.EMPTYSTRING;
    String nombre = Generales.EMPTYSTRING;
    String primerNombre = Generales.EMPTYSTRING;
    String tipoUsuario = Generales.EMPTYSTRING;
    String registradoPor = Generales.EMPTYSTRING;
    String fechaRegistro = Generales.EMPTYSTRING;
    String idTipoUsuario = Generales.EMPTYSTRING;
    String imagenPerfil = Generales.EMPTYSTRING;
    String correo = Generales.EMPTYSTRING;
    String celular = Generales.EMPTYSTRING;
    String documento = Generales.EMPTYSTRING;
    String clave = Generales.EMPTYSTRING;
    String municipio = Generales.EMPTYSTRING;
    String idMunicipio = Generales.EMPTYSTRING;
    String idTipoDocumento = Generales.EMPTYSTRING;
    String tipoDocumento = Generales.EMPTYSTRING;
    String apellido = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
    String departamento = Generales.EMPTYSTRING;
    String direccion = Generales.EMPTYSTRING;
    boolean Authenticated = false;
    String eficacia = Generales.EMPTYSTRING;
    String message = Generales.EMPTYSTRING;
    String idEmpresa = Generales.EMPTYSTRING;
    String eficiencia = Generales.ZERO;
    String calificacion = Generales.ZERO;
    String idActividad = Generales.EMPTYSTRING;
    String token = Generales.EMPTYSTRING;
    String cargo = Generales.EMPTYSTRING;

    /**
     *
     * @return
     */
    public String getCalificacion() {
        return calificacion;
    }

    /**
     *
     * @param calificacion
     */
    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    /**
     *
     * @return
     */
    public String getEficiencia() {
        return eficiencia;
    }

    /**
     *
     * @param eficiencia
     */
    public void setEficiencia(String eficiencia) {
        this.eficiencia = eficiencia;
    }

    /**
     *
     * @return
     */
    public String getEficacia() {
        return eficacia;
    }

    /**
     *
     * @param eficacia
     */
    public void setEficacia(String eficacia) {
        this.eficacia = eficacia;
    }

    /**
     *
     * @return
     */
    public String getIdEmpresa() {
        return idEmpresa;
    }

    /**
     *
     * @param idEmpresa
     */
    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     *
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param menssage
     */
    public void setMessage(String menssage) {
        this.message = menssage;
    }

    /**
     *
     * @return
     */
    public boolean isAuthenticated() {
        return Authenticated;
    }

    /**
     *
     * @param Authenticated
     */
    public void setAuthenticated(boolean Authenticated) {
        this.Authenticated = Authenticated;
    }

    /**
     *
     * @return
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     *
     * @param usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public String getTipoUsuario() {
        return tipoUsuario;
    }

    /**
     *
     * @param tipoUsuario
     */
    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }



    /**
     *
     * @return
     */
    public String getRegistradoPor() {
        return registradoPor;
    }

    /**
     *
     * @param registradoPor
     */
    public void setRegistradoPor(String registradoPor) {
        this.registradoPor = registradoPor;
    }

    /**
     *
     * @return
     */
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     *
     * @param fechaRegistro
     */
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    /**
     *
     * @return
     */
    public String getIdTipoUsuario() {
        return idTipoUsuario;
    }

    /**
     *
     * @param idTipoUsuario
     */
    public void setIdTipoUsuario(String idTipoUsuario) {
        this.idTipoUsuario = idTipoUsuario;
    }

    /**
     *
     * @return
     */
    public String getImagenPerfil() {
        return imagenPerfil;
    }

    /**
     *
     * @param imagenPerfil
     */
    public void setImagenPerfil(String imagenPerfil) {
        this.imagenPerfil = imagenPerfil;
    }

    /**
     *
     * @return
     */
    public String getCorreo() {
        return correo;
    }

    /**
     *
     * @param correo
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     *
     * @return
     */
    public String getCelular() {
        return celular;
    }

    /**
     *
     * @param celular
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     *
     * @return
     */
    public String getDocumento() {
        return documento;
    }

    /**
     *
     * @param documento
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     *
     * @return
     */
    public String getClave() {
        return clave;
    }

    /**
     *
     * @param clave
     */
    public void setClave(String clave) {
        this.clave = clave;
    }

    /**
     *
     * @return
     */
    public String getMunicipio() {
        return municipio;
    }

    /**
     *
     * @param municipio
     */
    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    /**
     *
     * @return
     */
    public String getIdMunicipio() {
        return idMunicipio;
    }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(String idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    /**
     *
     * @return
     */
    public String getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     *
     * @param idTipoDocumento
     */
    public void setIdTipoDocumento(String idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    /**
     *
     * @return
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     *
     * @param tipoDocumento
     */
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    /**
     *
     * @return
     */
    public String getApellido() {
        return apellido;
    }

    /**
     *
     * @param apellido
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     *
     * @return
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * @return
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     *
     * @param departamento
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     *
     * @return
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     *
     * @param direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     *
     * @return
     */
    public String getIdActividad() {
        return idActividad;
    }

    /**
     *
     * @param idActividad
     */
    public void setIdActividad(String idActividad) {
        this.idActividad = idActividad;
    }

    /**
     *
     * @return
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     */
    public String getCargo() {
        return cargo;
    }

    /**
     *
     * @param cargo
     */
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    /**
     *
     * @return
     */
    public String getPrimerNombre() {
        return primerNombre;
    }

    /**
     *
     * @param primerNombre
     */
    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }
    
    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
