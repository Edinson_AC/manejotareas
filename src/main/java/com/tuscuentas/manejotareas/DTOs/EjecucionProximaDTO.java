/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;
import java.io.Serializable;

/**
 *
 * @author diego
 */
public class EjecucionProximaDTO implements Serializable {

    String idEjecucionProxima = Generales.EMPTYSTRING;
    String fechaInicialProxima = Generales.EMPTYSTRING;
    String idTareaEmpresa = Generales.EMPTYSTRING;
    String codigoEjecucionProxima = Generales.EMPTYSTRING;
    String calendarioFecha = Generales.EMPTYSTRING;
    String idEmpresa = Generales.EMPTYSTRING;
    String diaHabilSiguiente = Generales.EMPTYSTRING;
    String impuesto = Generales.EMPTYSTRING;
    String idTareaPredeterminada = Generales.EMPTYSTRING;
    String digitos = Generales.EMPTYSTRING;

    /**
     *
     * @return
     */
    public String getIdEjecucionProxima() {
        return idEjecucionProxima;
    }

    /**
     *
     * @param idEjecucionProxima
     */
    public void setIdEjecucionProxima(String idEjecucionProxima) {
        this.idEjecucionProxima = idEjecucionProxima;
    }

    /**
     *
     * @return
     */
    public String getFechaInicialProxima() {
        return fechaInicialProxima;
    }

    /**
     *
     * @param fechaInicialProxima
     */
    public void setFechaInicialProxima(String fechaInicialProxima) {
        this.fechaInicialProxima = fechaInicialProxima;
    }

    /**
     *
     * @return
     */
    public String getIdTareaEmpresa() {
        return idTareaEmpresa;
    }

    /**
     *
     * @param idTareaEmpresa
     */
    public void setIdTareaEmpresa(String idTareaEmpresa) {
        this.idTareaEmpresa = idTareaEmpresa;
    }

    /**
     *
     * @return
     */
    public String getCodigoEjecucionProxima() {
        return codigoEjecucionProxima;
    }

    /**
     *
     * @param codigoEjecucionProxima
     */
    public void setCodigoEjecucionProxima(String codigoEjecucionProxima) {
        this.codigoEjecucionProxima = codigoEjecucionProxima;
    }

    /**
     *
     * @return
     */
    public String getCalendarioFecha() {
        return calendarioFecha;
    }

    /**
     *
     * @param calendarioFecha
     */
    public void setCalendarioFecha(String calendarioFecha) {
        this.calendarioFecha = calendarioFecha;
    }

    /**
     *
     * @return
     */
    public String getIdEmpresa() {
        return idEmpresa;
    }

    /**
     *
     * @param idEmpresa
     */
    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     *
     * @return
     */
    public String getDiaHabilSiguiente() {
        return diaHabilSiguiente;
    }

    /**
     *
     * @param diaHabilSiguiente
     */
    public void setDiaHabilSiguiente(String diaHabilSiguiente) {
        this.diaHabilSiguiente = diaHabilSiguiente;
    }

    /**
     *
     * @return
     */
    public String getImpuesto() {
        return impuesto;
    }

    /**
     *
     * @param impuesto
     */
    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    /**
     *
     * @return
     */
    public String getIdTareaPredeterminada() {
        return idTareaPredeterminada;
    }

    /**
     *
     * @param idTareaPredeterminada
     */
    public void setIdTareaPredeterminada(String idTareaPredeterminada) {
        this.idTareaPredeterminada = idTareaPredeterminada;
    }

    public String getDigitos() {
        return digitos;
    }

    public void setDigitos(String digitos) {
        this.digitos = digitos;
    }

    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
