/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class ActividadPredeterminadaDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String descripcion = Generales.EMPTYSTRING;
    String actividad = Generales.EMPTYSTRING;
    String diasDuracion = Generales.EMPTYSTRING;
    String diasProrroga = Generales.EMPTYSTRING;
    String cargoActividad = Generales.EMPTYSTRING;
    String fechaInicial = Generales.EMPTYSTRING;
    String fechaFinal = Generales.EMPTYSTRING;
    String idTarea = Generales.EMPTYSTRING;
    String ordenActividad = Generales.EMPTYSTRING;
    String idUsuarioPorCargo = Generales.EMPTYSTRING;
    String idCagoEmpresa = Generales.EMPTYSTRING;
    String tareaPredeterminada = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
        String descripcionCargo = Generales.EMPTYSTRING;

    ArrayList<CheckListDTO> checks = null;

    /**
     *
     * @return
     */
    public String getIdUsuarioPorCargo() {
        return idUsuarioPorCargo;
    }

    /**
     *
     * @param idUsuarioPorCargo
     */
    public void setIdUsuarioPorCargo(String idUsuarioPorCargo) {
        this.idUsuarioPorCargo = idUsuarioPorCargo;
    }

    /**
     *
     * @return
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     *
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     *
     * @return
     */
    public String getCargoActividad() {
        return cargoActividad;
    }

    /**
     *
     * @param cargoActividad
     */
    public void setCargoActividad(String cargoActividad) {
        this.cargoActividad = cargoActividad;
    }

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getActividad() {
        return actividad;
    }

    /**
     *
     * @param actividad
     */
    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    /**
     *
     * @return
     */
    public String getDiasDuracion() {
        return diasDuracion;
    }

    /**
     *
     * @param diasDuracion
     */
    public void setDiasDuracion(String diasDuracion) {
        this.diasDuracion = diasDuracion;
    }

    /**
     *
     * @return
     */
    public String getDiasProrroga() {
        return diasProrroga;
    }

    /**
     *
     * @param diasProrroga
     */
    public void setDiasProrroga(String diasProrroga) {
        this.diasProrroga = diasProrroga;
    }

    /**
     *
     * @return
     */
    public String getFechaInicial() {
        return fechaInicial;
    }

    /**
     *
     * @param fechaInicial
     */
    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    /**
     *
     * @return
     */
    public String getFechaFinal() {
        return fechaFinal;
    }

    /**
     *
     * @param fechaFinal
     */
    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    /**
     *
     * @return
     */
    public String getIdTarea() {
        return idTarea;
    }

    /**
     *
     * @param idTarea
     */
    public void setIdTarea(String idTarea) {
        this.idTarea = idTarea;
    }

    /**
     *
     * @return
     */
    public String getOrdenActividad() {
        return ordenActividad;
    }

    /**
     *
     * @param ordenActividad
     */
    public void setOrdenActividad(String ordenActividad) {
        this.ordenActividad = ordenActividad;
    }

    /**
     *
     * @return
     */
    public String getIdCagoEmpresa() {
        return idCagoEmpresa;
    }

    /**
     *
     * @param idCagoEmpresa
     */
    public void setIdCagoEmpresa(String idCagoEmpresa) {
        this.idCagoEmpresa = idCagoEmpresa;
    }

    /**
     *
     * @return
     */
    public String getTareaPredeterminada() {
        return tareaPredeterminada;
    }

    /**
     *
     * @param tareaPredeterminada
     */
    public void setTareaPredeterminada(String tareaPredeterminada) {
        this.tareaPredeterminada = tareaPredeterminada;
    }

    /**
     *
     * @return
     */
    public ArrayList<CheckListDTO> getChecks() {
        return checks;
    }

    /**
     *
     * @param checks
     */
    public void setChecks(ArrayList<CheckListDTO> checks) {
        this.checks = checks;
    }

    /**
     *
     * @return
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcionCargo() {
        return descripcionCargo;
    }

    public void setDescripcionCargo(String descripcionCargo) {
        this.descripcionCargo = descripcionCargo;
    }

    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
