/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class EjecucionTareaDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String idActividadPre = Generales.EMPTYSTRING;
    String idTarea = Generales.EMPTYSTRING;
    String descripcionTarea = Generales.EMPTYSTRING;
    String codEjcucion = Generales.EMPTYSTRING;
    String fechaInicioEjecucion = Generales.EMPTYSTRING;
    String fechaFinEjecucion = Generales.EMPTYSTRING;
    String nombreTarea = Generales.EMPTYSTRING;
    String nombreActividad = Generales.EMPTYSTRING;
    String nombreEmpresa = Generales.EMPTYSTRING;
    String idTareaEmpresa = Generales.EMPTYSTRING;
    String orden = Generales.EMPTYSTRING;
    String observacion = Generales.EMPTYSTRING;
    String idUsuario = Generales.EMPTYSTRING;
    ArrayList<CheckListDTO> checkList = new ArrayList<>();
    String ejecucionLeida = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
    String estadoProrroga = Generales.EMPTYSTRING;
    String estadoEjecucionAnterior = Generales.EMPTYSTRING;
    String fechaFinalizada = Generales.EMPTYSTRING;
    String diasEstimados = Generales.EMPTYSTRING;
    String diasUsados = Generales.EMPTYSTRING;
    String idEmpresa = Generales.EMPTYSTRING;
    String nombreComercialEmpresa = Generales.EMPTYSTRING;
    String dias = Generales.EMPTYSTRING;
    String diasProrroga = Generales.EMPTYSTRING;
    String estadoDias = Generales.EMPTYSTRING;
    String cargoResponsable = Generales.EMPTYSTRING;
    String usuarioResponsable = Generales.EMPTYSTRING;
    String habil = Generales.EMPTYSTRING;
    String userSiigo = Generales.EMPTYSTRING;
    String passwordSiigo = Generales.EMPTYSTRING;
    String imgUsuario = Generales.EMPTYSTRING;
    String emailResponsable = Generales.EMPTYSTRING;

    /**
     *
     * @return
     */
    public String getIdActividadPre() {
        return idActividadPre;
    }

    /**
     *
     * @param idActividadPre
     */
    public void setIdActividadPre(String idActividadPre) {
        this.idActividadPre = idActividadPre;
    }

    /**
     *
     * @return
     */
    public String getIdEmpresa() {
        return idEmpresa;
    }

    /**
     *
     * @param idEmpresa
     */
    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     *
     * @return
     */
    public String getDiasEstimados() {
        return diasEstimados;
    }

    /**
     *
     * @param diasEstimados
     */
    public void setDiasEstimados(String diasEstimados) {
        this.diasEstimados = diasEstimados;
    }

    /**
     *
     * @return
     */
    public String getDiasUsados() {
        return diasUsados;
    }

    /**
     *
     * @param diasUsados
     */
    public void setDiasUsados(String diasUsados) {
        this.diasUsados = diasUsados;
    }

    /**
     *
     * @return
     */
    public String getFechaFinalizada() {
        return fechaFinalizada;
    }

    /**
     *
     * @param fechaFinalizada
     */
    public void setFechaFinalizada(String fechaFinalizada) {
        this.fechaFinalizada = fechaFinalizada;
    }

    /**
     *
     * @return
     */
    public String getEstadoEjecucionAnterior() {
        return estadoEjecucionAnterior;
    }

    /**
     *
     * @param estadoEjecucionAnterior
     */
    public void setEstadoEjecucionAnterior(String estadoEjecucionAnterior) {
        this.estadoEjecucionAnterior = estadoEjecucionAnterior;
    }

    /**
     *
     * @return
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     *
     * @param observacion
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     *
     * @return
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public String getOrden() {
        return orden;
    }

    /**
     *
     * @param orden
     */
    public void setOrden(String orden) {
        this.orden = orden;
    }

    /**
     *
     * @return
     */
    public String getDescripcionTarea() {
        return descripcionTarea;
    }

    /**
     *
     * @param descripcionTarea
     */
    public void setDescripcionTarea(String descripcionTarea) {
        this.descripcionTarea = descripcionTarea;
    }

    /**
     *
     * @return
     */
    public ArrayList<CheckListDTO> getCheckList() {
        return checkList;
    }

    /**
     *
     * @param checkList
     */
    public void setCheckList(ArrayList<CheckListDTO> checkList) {
        this.checkList = checkList;
    }

    ArrayList<ActividadPredeterminadaDTO> actividades = null;

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getIdTarea() {
        return idTarea;
    }

    /**
     *
     * @param idTarea
     */
    public void setIdTarea(String idTarea) {
        this.idTarea = idTarea;
    }

    /**
     *
     * @return
     */
    public String getCodEjcucion() {
        return codEjcucion;
    }

    /**
     *
     * @param codEjcucion
     */
    public void setCodEjcucion(String codEjcucion) {
        this.codEjcucion = codEjcucion;
    }

    /**
     *
     * @return
     */
    public String getFechaInicioEjecucion() {
        return fechaInicioEjecucion;
    }

    /**
     *
     * @param fechaInicioEjecucion
     */
    public void setFechaInicioEjecucion(String fechaInicioEjecucion) {
        this.fechaInicioEjecucion = fechaInicioEjecucion;
    }

    /**
     *
     * @return
     */
    public ArrayList<ActividadPredeterminadaDTO> getActividades() {
        return actividades;
    }

    /**
     *
     * @param actividades
     */
    public void setActividades(ArrayList<ActividadPredeterminadaDTO> actividades) {
        this.actividades = actividades;
    }

    /**
     *
     * @return
     */
    public String getFechaFinEjecucion() {
        return fechaFinEjecucion;
    }

    /**
     *
     * @param fechaFinEjecucion
     */
    public void setFechaFinEjecucion(String fechaFinEjecucion) {
        this.fechaFinEjecucion = fechaFinEjecucion;
    }

    /**
     *
     * @return
     */
    public String getNombreTarea() {
        return nombreTarea;
    }

    /**
     *
     * @param nombreTarea
     */
    public void setNombreTarea(String nombreTarea) {
        this.nombreTarea = nombreTarea;
    }

    /**
     *
     * @return
     */
    public String getNombreActividad() {
        return nombreActividad;
    }

    /**
     *
     * @param nombreActividad
     */
    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    /**
     *
     * @return
     */
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    /**
     *
     * @param nombreEmpresa
     */
    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    /**
     *
     * @return
     */
    public String getIdTareaEmpresa() {
        return idTareaEmpresa;
    }

    /**
     *
     * @param idTareaEmpresa
     */
    public void setIdTareaEmpresa(String idTareaEmpresa) {
        this.idTareaEmpresa = idTareaEmpresa;
    }

    /**
     *
     * @return
     */
    public String getEjecucionLeida() {
        return ejecucionLeida;
    }

    /**
     *
     * @param ejecucionLeida
     */
    public void setEjecucionLeida(String ejecucionLeida) {
        this.ejecucionLeida = ejecucionLeida;
    }

    /**
     *
     * @return
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * @return
     */
    public String getEstadoProrroga() {
        return estadoProrroga;
    }

    /**
     *
     * @param estadoProrroga
     */
    public void setEstadoProrroga(String estadoProrroga) {
        this.estadoProrroga = estadoProrroga;
    }

    /**
     *
     * @return
     */
    public String getNombreComercialEmpresa() {
        return nombreComercialEmpresa;
    }

    /**
     *
     * @param nombreComercialEmpresa
     */
    public void setNombreComercialEmpresa(String nombreComercialEmpresa) {
        this.nombreComercialEmpresa = nombreComercialEmpresa;
    }

    /**
     *
     * @return
     */
    public String getDias() {
        return dias;
    }

    /**
     *
     * @param dias
     */
    public void setDias(String dias) {
        this.dias = dias;
    }

    /**
     *
     * @return
     */
    public String getEstadoDias() {
        return estadoDias;
    }

    /**
     *
     * @param estadoDias
     */
    public void setEstadoDias(String estadoDias) {
        this.estadoDias = estadoDias;
    }

    /**
     *
     * @return
     */
    public String getCargoResponsable() {
        return cargoResponsable;
    }

    /**
     *
     * @param cargoResponsable
     */
    public void setCargoResponsable(String cargoResponsable) {
        this.cargoResponsable = cargoResponsable;
    }

    /**
     *
     * @return
     */
    public String getUsuarioResponsable() {
        return usuarioResponsable;
    }

    /**
     *
     * @param usuarioResponsable
     */
    public void setUsuarioResponsable(String usuarioResponsable) {
        this.usuarioResponsable = usuarioResponsable;
    }

    /**
     *
     * @return
     */
    public String getDiasProrroga() {
        return diasProrroga;
    }

    /**
     *
     * @param diasProrroga
     */
    public void setDiasProrroga(String diasProrroga) {
        this.diasProrroga = diasProrroga;
    }

    /**
     *
     * @return
     */
    public String getHabil() {
        return habil;
    }

    /**
     *
     * @param habil
     */
    public void setHabil(String habil) {
        this.habil = habil;
    }

    /**
     *
     * @return
     */
    public String getUserSiigo() {
        return userSiigo;
    }

    /**
     *
     * @param userSiigo
     */
    public void setUserSiigo(String userSiigo) {
        this.userSiigo = userSiigo;
    }

    /**
     *
     * @return
     */
    public String getPasswordSiigo() {
        return passwordSiigo;
    }

    /**
     *
     * @param passwordSiigo
     */
    public void setPasswordSiigo(String passwordSiigo) {
        this.passwordSiigo = passwordSiigo;
    }

    /**
     *
     * @return
     */
    public String getImgUsuario() {
        return imgUsuario;
    }

    /**
     *
     * @param imgUsuario
     */
    public void setImgUsuario(String imgUsuario) {
        this.imgUsuario = imgUsuario;
    }

    /**
     *
     * @return
     */
    public String getEmailResponsable() {
        return emailResponsable;
    }

    /**
     *
     * @param emailResponsable
     */
    public void setEmailResponsable(String emailResponsable) {
        this.emailResponsable = emailResponsable;
    }

    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
