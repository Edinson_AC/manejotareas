/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class TareaPredeterminadaDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String idEjecucion = Generales.EMPTYSTRING;
    String procesoTarea = Generales.EMPTYSTRING;
    String nombreTarea = Generales.EMPTYSTRING;
    String descripcionTarea = Generales.EMPTYSTRING;
    String esquemaTarea = Generales.EMPTYSTRING;
    String calendarioFecha = Generales.EMPTYSTRING;
    String calendarioTrabutario = Generales.EMPTYSTRING;
    String nacional = Generales.EMPTYSTRING;
    String distrital = Generales.EMPTYSTRING;
    String comentarioobligado = Generales.EMPTYSTRING;
    String recurrente = Generales.EMPTYSTRING;
    String tipoRecurrencia = Generales.EMPTYSTRING;
    String editable = Generales.EMPTYSTRING;
    String idPasoEjecucion = Generales.EMPTYSTRING;
    String estadoPasoEjecucion = Generales.EMPTYSTRING;
    String cargoEjecucion = Generales.EMPTYSTRING;
    String diasEjecucion = Generales.EMPTYSTRING;
    String prorrogaEjecucion = Generales.EMPTYSTRING;
    String idPasoRevision = Generales.EMPTYSTRING;
    String estadoPasoRevision = Generales.EMPTYSTRING;
    String cargoRevision = Generales.EMPTYSTRING;
    String diasRevision = Generales.EMPTYSTRING;
    String prorrogaRevision = Generales.EMPTYSTRING;
    String idPasoAprobacion = Generales.EMPTYSTRING;
    String estadoPasoAprobacion = Generales.EMPTYSTRING;
    String cargoAprobacion = Generales.EMPTYSTRING;
    String diasAprobacion = Generales.EMPTYSTRING;
    String prorrogaAprobacion = Generales.EMPTYSTRING;
    String idPasoRecepcion = Generales.EMPTYSTRING;
    String estadoPasoRecepcion = Generales.EMPTYSTRING;
    String cargoRecepcion = Generales.EMPTYSTRING;
    String diasRecepcion = Generales.EMPTYSTRING;
    String prorrogaRecepcion = Generales.EMPTYSTRING;
    String idTipoempresa = Generales.EMPTYSTRING;
    String fechaCalendarioTributario = Generales.EMPTYSTRING;
    String fechaFinalTusCuentas = Generales.EMPTYSTRING;
    String idEmpresa = Generales.EMPTYSTRING;
    String fechaInicialReal = Generales.EMPTYSTRING;
    String fechaFinalReal = Generales.EMPTYSTRING;
    int duracionConProrroga = Generales.ZEROVALUE;
    String tareaGerente = Generales.EMPTYSTRING;
    String usuarioRegistra = Generales.EMPTYSTRING;
    String impuesto = Generales.EMPTYSTRING;
    String diaHabilSiguiente = Generales.EMPTYSTRING;
    String nombreEjecucion = Generales.EMPTYSTRING;
    String prorroga = Generales.EMPTYSTRING;
    String idActividad = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
    String nueva = Generales.EMPTYSTRING;
    String tareaHabil = Generales.EMPTYSTRING;
    String ejecucionHabil = Generales.EMPTYSTRING;
    String codigoEjecucion = Generales.EMPTYSTRING;
    String pideGerente = Generales.EMPTYSTRING;
    String nombreArea = Generales.EMPTYSTRING;
    String estadoEjecucion = Generales.EMPTYSTRING;
    String estadoRevision = Generales.EMPTYSTRING;
    String estadoAprobacion = Generales.EMPTYSTRING;
    String estadoRecepcion = Generales.EMPTYSTRING;
    String ejecucion = Generales.EMPTYSTRING;
    String revision = Generales.EMPTYSTRING;
    String aprobacion = Generales.EMPTYSTRING;
    String recepcion = Generales.EMPTYSTRING;
    String codTareaPredetermianda = Generales.EMPTYSTRING;
    String duplicar = Generales.EMPTYSTRING;

    ArrayList<ActividadPredeterminadaDTO> actividades = null;
    ArrayList<EjecucionTareaDTO> ejecuciones = null;
    ArrayList arrayarrays = null;

    String idTareaEmpresa = Generales.EMPTYSTRING;
    String idUsuario = Generales.EMPTYSTRING;
    String diasAntes = Generales.EMPTYSTRING;
    String diasMes = Generales.EMPTYSTRING;

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getIdEjecucion() {
        return idEjecucion;
    }

    /**
     *
     * @param idEjecucion
     */
    public void setIdEjecucion(String idEjecucion) {
        this.idEjecucion = idEjecucion;
    }

    /**
     *
     * @return
     */
    public String getProcesoTarea() {
        return procesoTarea;
    }

    /**
     *
     * @param procesoTarea
     */
    public void setProcesoTarea(String procesoTarea) {
        this.procesoTarea = procesoTarea;
    }

    /**
     *
     * @return
     */
    public String getNombreTarea() {
        return nombreTarea;
    }

    /**
     *
     * @param nombreTarea
     */
    public void setNombreTarea(String nombreTarea) {
        this.nombreTarea = nombreTarea;
    }

    /**
     *
     * @return
     */
    public String getDescripcionTarea() {
        return descripcionTarea;
    }

    /**
     *
     * @param descripcionTarea
     */
    public void setDescripcionTarea(String descripcionTarea) {
        this.descripcionTarea = descripcionTarea;
    }

    /**
     *
     * @return
     */
    public String getEsquemaTarea() {
        return esquemaTarea;
    }

    /**
     *
     * @param esquemaTarea
     */
    public void setEsquemaTarea(String esquemaTarea) {
        this.esquemaTarea = esquemaTarea;
    }

    /**
     *
     * @return
     */
    public String getCalendarioFecha() {
        return calendarioFecha;
    }

    /**
     *
     * @param calendarioFecha
     */
    public void setCalendarioFecha(String calendarioFecha) {
        this.calendarioFecha = calendarioFecha;
    }

    /**
     *
     * @return
     */
    public String getCalendarioTrabutario() {
        return calendarioTrabutario;
    }

    /**
     *
     * @param calendarioTrabutario
     */
    public void setCalendarioTrabutario(String calendarioTrabutario) {
        this.calendarioTrabutario = calendarioTrabutario;
    }

    /**
     *
     * @return
     */
    public String getNacional() {
        return nacional;
    }

    /**
     *
     * @param nacional
     */
    public void setNacional(String nacional) {
        this.nacional = nacional;
    }

    /**
     *
     * @return
     */
    public String getDistrital() {
        return distrital;
    }

    /**
     *
     * @param distrital
     */
    public void setDistrital(String distrital) {
        this.distrital = distrital;
    }

    /**
     *
     * @return
     */
    public String getComentarioobligado() {
        return comentarioobligado;
    }

    /**
     *
     * @param comentarioobligado
     */
    public void setComentarioobligado(String comentarioobligado) {
        this.comentarioobligado = comentarioobligado;
    }

    /**
     *
     * @return
     */
    public String getRecurrente() {
        return recurrente;
    }

    /**
     *
     * @param recurrente
     */
    public void setRecurrente(String recurrente) {
        this.recurrente = recurrente;
    }

    /**
     *
     * @return
     */
    public String getTipoRecurrencia() {
        return tipoRecurrencia;
    }

    /**
     *
     * @param tipoRecurrencia
     */
    public void setTipoRecurrencia(String tipoRecurrencia) {
        this.tipoRecurrencia = tipoRecurrencia;
    }

    /**
     *
     * @return
     */
    public String getEditable() {
        return editable;
    }

    /**
     *
     * @param editable
     */
    public void setEditable(String editable) {
        this.editable = editable;
    }

    /**
     *
     * @return
     */
    public String getIdPasoEjecucion() {
        return idPasoEjecucion;
    }

    /**
     *
     * @param idPasoEjecucion
     */
    public void setIdPasoEjecucion(String idPasoEjecucion) {
        this.idPasoEjecucion = idPasoEjecucion;
    }

    /**
     *
     * @return
     */
    public String getEstadoPasoEjecucion() {
        return estadoPasoEjecucion;
    }

    /**
     *
     * @param estadoPasoEjecucion
     */
    public void setEstadoPasoEjecucion(String estadoPasoEjecucion) {
        this.estadoPasoEjecucion = estadoPasoEjecucion;
    }

    /**
     *
     * @return
     */
    public String getCargoEjecucion() {
        return cargoEjecucion;
    }

    /**
     *
     * @param cargoEjecucion
     */
    public void setCargoEjecucion(String cargoEjecucion) {
        this.cargoEjecucion = cargoEjecucion;
    }

    /**
     *
     * @return
     */
    public String getDiasEjecucion() {
        return diasEjecucion;
    }

    /**
     *
     * @param diasEjecucion
     */
    public void setDiasEjecucion(String diasEjecucion) {
        this.diasEjecucion = diasEjecucion;
    }

    /**
     *
     * @return
     */
    public String getProrrogaEjecucion() {
        return prorrogaEjecucion;
    }

    /**
     *
     * @param prorrogaEjecucion
     */
    public void setProrrogaEjecucion(String prorrogaEjecucion) {
        this.prorrogaEjecucion = prorrogaEjecucion;
    }

    /**
     *
     * @return
     */
    public String getIdPasoRevision() {
        return idPasoRevision;
    }

    /**
     *
     * @param idPasoRevision
     */
    public void setIdPasoRevision(String idPasoRevision) {
        this.idPasoRevision = idPasoRevision;
    }

    /**
     *
     * @return
     */
    public String getEstadoPasoRevision() {
        return estadoPasoRevision;
    }

    /**
     *
     * @param estadoPasoRevision
     */
    public void setEstadoPasoRevision(String estadoPasoRevision) {
        this.estadoPasoRevision = estadoPasoRevision;
    }

    /**
     *
     * @return
     */
    public String getCargoRevision() {
        return cargoRevision;
    }

    /**
     *
     * @param cargoRevision
     */
    public void setCargoRevision(String cargoRevision) {
        this.cargoRevision = cargoRevision;
    }

    /**
     *
     * @return
     */
    public String getDiasRevision() {
        return diasRevision;
    }

    /**
     *
     * @param diasRevision
     */
    public void setDiasRevision(String diasRevision) {
        this.diasRevision = diasRevision;
    }

    /**
     *
     * @return
     */
    public String getProrrogaRevision() {
        return prorrogaRevision;
    }

    /**
     *
     * @param prorrogaRevision
     */
    public void setProrrogaRevision(String prorrogaRevision) {
        this.prorrogaRevision = prorrogaRevision;
    }

    /**
     *
     * @return
     */
    public String getIdPasoAprobacion() {
        return idPasoAprobacion;
    }

    /**
     *
     * @param idPasoAprobacion
     */
    public void setIdPasoAprobacion(String idPasoAprobacion) {
        this.idPasoAprobacion = idPasoAprobacion;
    }

    /**
     *
     * @return
     */
    public String getEstadoPasoAprobacion() {
        return estadoPasoAprobacion;
    }

    /**
     *
     * @param estadoPasoAprobacion
     */
    public void setEstadoPasoAprobacion(String estadoPasoAprobacion) {
        this.estadoPasoAprobacion = estadoPasoAprobacion;
    }

    /**
     *
     * @return
     */
    public String getCargoAprobacion() {
        return cargoAprobacion;
    }

    /**
     *
     * @param cargoAprobacion
     */
    public void setCargoAprobacion(String cargoAprobacion) {
        this.cargoAprobacion = cargoAprobacion;
    }

    /**
     *
     * @return
     */
    public String getDiasAprobacion() {
        return diasAprobacion;
    }

    /**
     *
     * @param diasAprobacion
     */
    public void setDiasAprobacion(String diasAprobacion) {
        this.diasAprobacion = diasAprobacion;
    }

    /**
     *
     * @return
     */
    public String getProrrogaAprobacion() {
        return prorrogaAprobacion;
    }

    /**
     *
     * @param prorrogaAprobacion
     */
    public void setProrrogaAprobacion(String prorrogaAprobacion) {
        this.prorrogaAprobacion = prorrogaAprobacion;
    }

    /**
     *
     * @return
     */
    public String getIdPasoRecepcion() {
        return idPasoRecepcion;
    }

    /**
     *
     * @param idPasoRecepcion
     */
    public void setIdPasoRecepcion(String idPasoRecepcion) {
        this.idPasoRecepcion = idPasoRecepcion;
    }

    /**
     *
     * @return
     */
    public String getEstadoPasoRecepcion() {
        return estadoPasoRecepcion;
    }

    /**
     *
     * @param estadoPasoRecepcion
     */
    public void setEstadoPasoRecepcion(String estadoPasoRecepcion) {
        this.estadoPasoRecepcion = estadoPasoRecepcion;
    }

    /**
     *
     * @return
     */
    public String getCargoRecepcion() {
        return cargoRecepcion;
    }

    /**
     *
     * @param cargoRecepcion
     */
    public void setCargoRecepcion(String cargoRecepcion) {
        this.cargoRecepcion = cargoRecepcion;
    }

    /**
     *
     * @return
     */
    public String getDiasRecepcion() {
        return diasRecepcion;
    }

    /**
     *
     * @param diasRecepcion
     */
    public void setDiasRecepcion(String diasRecepcion) {
        this.diasRecepcion = diasRecepcion;
    }

    /**
     *
     * @return
     */
    public String getProrrogaRecepcion() {
        return prorrogaRecepcion;
    }

    /**
     *
     * @param prorrogaRecepcion
     */
    public void setProrrogaRecepcion(String prorrogaRecepcion) {
        this.prorrogaRecepcion = prorrogaRecepcion;
    }

    /**
     *
     * @return
     */
    public String getIdTipoempresa() {
        return idTipoempresa;
    }

    /**
     *
     * @param idTipoempresa
     */
    public void setIdTipoempresa(String idTipoempresa) {
        this.idTipoempresa = idTipoempresa;
    }

    /**
     *
     * @return
     */
    public String getFechaCalendarioTributario() {
        return fechaCalendarioTributario;
    }

    /**
     *
     * @param fechaCalendarioTributario
     */
    public void setFechaCalendarioTributario(String fechaCalendarioTributario) {
        this.fechaCalendarioTributario = fechaCalendarioTributario;
    }

    /**
     *
     * @return
     */
    public String getFechaFinalTusCuentas() {
        return fechaFinalTusCuentas;
    }

    /**
     *
     * @param fechaFinalTusCuentas
     */
    public void setFechaFinalTusCuentas(String fechaFinalTusCuentas) {
        this.fechaFinalTusCuentas = fechaFinalTusCuentas;
    }

    /**
     *
     * @return
     */
    public String getIdEmpresa() {
        return idEmpresa;
    }

    /**
     *
     * @param idEmpresa
     */
    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     *
     * @return
     */
    public String getFechaInicialReal() {
        return fechaInicialReal;
    }

    /**
     *
     * @param fechaInicialReal
     */
    public void setFechaInicialReal(String fechaInicialReal) {
        this.fechaInicialReal = fechaInicialReal;
    }

    /**
     *
     * @return
     */
    public String getFechaFinalReal() {
        return fechaFinalReal;
    }

    /**
     *
     * @param fechaFinalReal
     */
    public void setFechaFinalReal(String fechaFinalReal) {
        this.fechaFinalReal = fechaFinalReal;
    }

    /**
     *
     * @return
     */
    public int getDuracionConProrroga() {
        return duracionConProrroga;
    }

    /**
     *
     * @param duracionConProrroga
     */
    public void setDuracionConProrroga(int duracionConProrroga) {
        this.duracionConProrroga = duracionConProrroga;
    }

    /**
     *
     * @return
     */
    public String getTareaGerente() {
        return tareaGerente;
    }

    /**
     *
     * @param tareaGerente
     */
    public void setTareaGerente(String tareaGerente) {
        this.tareaGerente = tareaGerente;
    }

    /**
     *
     * @return
     */
    public String getUsuarioRegistra() {
        return usuarioRegistra;
    }

    /**
     *
     * @param usuarioRegistra
     */
    public void setUsuarioRegistra(String usuarioRegistra) {
        this.usuarioRegistra = usuarioRegistra;
    }

    /**
     *
     * @return
     */
    public String getImpuesto() {
        return impuesto;
    }

    /**
     *
     * @param impuesto
     */
    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    /**
     *
     * @return
     */
    public String getDiaHabilSiguiente() {
        return diaHabilSiguiente;
    }

    /**
     *
     * @param diaHabilSiguiente
     */
    public void setDiaHabilSiguiente(String diaHabilSiguiente) {
        this.diaHabilSiguiente = diaHabilSiguiente;
    }

    /**
     *
     * @return
     */
    public String getNombreEjecucion() {
        return nombreEjecucion;
    }

    /**
     *
     * @param nombreEjecucion
     */
    public void setNombreEjecucion(String nombreEjecucion) {
        this.nombreEjecucion = nombreEjecucion;
    }

    /**
     *
     * @return
     */
    public String getProrroga() {
        return prorroga;
    }

    /**
     *
     * @param prorroga
     */
    public void setProrroga(String prorroga) {
        this.prorroga = prorroga;
    }

    /**
     *
     * @return
     */
    public String getIdActividad() {
        return idActividad;
    }

    /**
     *
     * @param idActividad
     */
    public void setIdActividad(String idActividad) {
        this.idActividad = idActividad;
    }

    /**
     *
     * @return
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * @return
     */
    public String getNueva() {
        return nueva;
    }

    /**
     *
     * @param nueva
     */
    public void setNueva(String nueva) {
        this.nueva = nueva;
    }

    /**
     *
     * @return
     */
    public String getTareaHabil() {
        return tareaHabil;
    }

    /**
     *
     * @param tareaHabil
     */
    public void setTareaHabil(String tareaHabil) {
        this.tareaHabil = tareaHabil;
    }

    /**
     *
     * @return
     */
    public String getEjecucionHabil() {
        return ejecucionHabil;
    }

    /**
     *
     * @param ejecucionHabil
     */
    public void setEjecucionHabil(String ejecucionHabil) {
        this.ejecucionHabil = ejecucionHabil;
    }

    /**
     *
     * @return
     */
    public String getCodigoEjecucion() {
        return codigoEjecucion;
    }

    /**
     *
     * @param codigoEjecucion
     */
    public void setCodigoEjecucion(String codigoEjecucion) {
        this.codigoEjecucion = codigoEjecucion;
    }

    /**
     *
     * @return
     */
    public String getPideGerente() {
        return pideGerente;
    }

    /**
     *
     * @param pideGerente
     */
    public void setPideGerente(String pideGerente) {
        this.pideGerente = pideGerente;
    }

    /**
     *
     * @return
     */
    public String getNombreArea() {
        return nombreArea;
    }

    /**
     *
     * @param nombreArea
     */
    public void setNombreArea(String nombreArea) {
        this.nombreArea = nombreArea;
    }

    /**
     *
     * @return
     */
    public String getEstadoEjecucion() {
        return estadoEjecucion;
    }

    /**
     *
     * @param estadoEjecucion
     */
    public void setEstadoEjecucion(String estadoEjecucion) {
        this.estadoEjecucion = estadoEjecucion;
    }

    /**
     *
     * @return
     */
    public String getEstadoRevision() {
        return estadoRevision;
    }

    /**
     *
     * @param estadoRevision
     */
    public void setEstadoRevision(String estadoRevision) {
        this.estadoRevision = estadoRevision;
    }

    /**
     *
     * @return
     */
    public String getEstadoAprobacion() {
        return estadoAprobacion;
    }

    /**
     *
     * @param estadoAprobacion
     */
    public void setEstadoAprobacion(String estadoAprobacion) {
        this.estadoAprobacion = estadoAprobacion;
    }

    /**
     *
     * @return
     */
    public String getEstadoRecepcion() {
        return estadoRecepcion;
    }

    /**
     *
     * @param estadoRecepcion
     */
    public void setEstadoRecepcion(String estadoRecepcion) {
        this.estadoRecepcion = estadoRecepcion;
    }

    /**
     *
     * @return
     */
    public String getCodTareaPredetermianda() {
        return codTareaPredetermianda;
    }

    /**
     *
     * @param codTareaPredetermianda
     */
    public void setCodTareaPredetermianda(String codTareaPredetermianda) {
        this.codTareaPredetermianda = codTareaPredetermianda;
    }

    /**
     *
     * @return
     */
    public ArrayList<ActividadPredeterminadaDTO> getActividades() {
        return actividades;
    }

    /**
     *
     * @param actividades
     */
    public void setActividades(ArrayList<ActividadPredeterminadaDTO> actividades) {
        this.actividades = actividades;
    }

    /**
     *
     * @return
     */
    public ArrayList<EjecucionTareaDTO> getEjecuciones() {
        return ejecuciones;
    }

    /**
     *
     * @param ejecuciones
     */
    public void setEjecuciones(ArrayList<EjecucionTareaDTO> ejecuciones) {
        this.ejecuciones = ejecuciones;
    }

    /**
     *
     * @return
     */
    public ArrayList getArrayarrays() {
        return arrayarrays;
    }

    /**
     *
     * @param arrayarrays
     */
    public void setArrayarrays(ArrayList arrayarrays) {
        this.arrayarrays = arrayarrays;
    }

    /**
     *
     * @return
     */
    public String getIdTareaEmpresa() {
        return idTareaEmpresa;
    }

    /**
     *
     * @param idTareaEmpresa
     */
    public void setIdTareaEmpresa(String idTareaEmpresa) {
        this.idTareaEmpresa = idTareaEmpresa;
    }

    /**
     *
     * @return
     */
    public String getEjecucion() {
        return ejecucion;
    }

    /**
     *
     * @param ejecucion
     */
    public void setEjecucion(String ejecucion) {
        this.ejecucion = ejecucion;
    }

    /**
     *
     * @return
     */
    public String getRevision() {
        return revision;
    }

    /**
     *
     * @param revision
     */
    public void setRevision(String revision) {
        this.revision = revision;
    }

    /**
     *
     * @return
     */
    public String getAprobacion() {
        return aprobacion;
    }

    /**
     *
     * @param aprobacion
     */
    public void setAprobacion(String aprobacion) {
        this.aprobacion = aprobacion;
    }

    /**
     *
     * @return
     */
    public String getRecepcion() {
        return recepcion;
    }

    /**
     *
     * @param recepcion
     */
    public void setRecepcion(String recepcion) {
        this.recepcion = recepcion;
    }

    /**
     *
     * @return
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public String getDuplicar() {
        return duplicar;
    }

    /**
     *
     * @param duplicar
     */
    public void setDuplicar(String duplicar) {
        this.duplicar = duplicar;
    }

    public String getDiasAntes() {
        return diasAntes;
    }

    public void setDiasAntes(String diasAntes) {
        this.diasAntes = diasAntes;
    }

    public String getDiasMes() {
        return diasMes;
    }

    public void setDiasMes(String diasMes) {
        this.diasMes = diasMes;
    }
    
    

    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
