/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author DIEGO
 */
public class EmpresaDTO implements Serializable {

    String id = Generales.EMPTYSTRING;
    String nombre = Generales.EMPTYSTRING;
    String nombreComercial = Generales.EMPTYSTRING;
    String primerNombre = Generales.EMPTYSTRING;
    String otrosNombre = Generales.EMPTYSTRING;
    String primerApellido = Generales.EMPTYSTRING;
    String segundoApellido = Generales.EMPTYSTRING;
    String numeroContrato = Generales.EMPTYSTRING;
    String fechaInicial = Generales.EMPTYSTRING;
    String fechaFinal = Generales.EMPTYSTRING;
    String celular = Generales.EMPTYSTRING;
    String direccion = Generales.EMPTYSTRING;
    String correo = Generales.EMPTYSTRING;
    String telefono = Generales.EMPTYSTRING;
    String estado = Generales.ESTADO_INACTIVO;
    String nit = Generales.EMPTYSTRING;
    String codigoVerificacion = Generales.EMPTYSTRING;
    String idContador = Generales.EMPTYSTRING;
    String idTipoDocumento = Generales.EMPTYSTRING;
    String idTipoRegimen = Generales.EMPTYSTRING;
    String documento = Generales.EMPTYSTRING;
    String representanteLegal = Generales.EMPTYSTRING;
    String idMunicipio = Generales.EMPTYSTRING;
    String idDepartamento = Generales.EMPTYSTRING;
    String municipio = Generales.EMPTYSTRING;
    String departamento = Generales.EMPTYSTRING;
    String personaJuridica = Generales.EMPTYSTRING;
    String personaNatural = Generales.EMPTYSTRING;
    String idTipoEmpresa = Generales.EMPTYSTRING;
    String registradoPor = Generales.EMPTYSTRING;
    String fechaRegistro = Generales.EMPTYSTRING;
    String dialaboralSabado = Generales.EMPTYSTRING;
    String dialaboralDomingo = Generales.EMPTYSTRING;
    String dialaboralFestivos = Generales.EMPTYSTRING;
    String cantidad = Generales.EMPTYSTRING;
    String calificacionContador = Generales.EMPTYSTRING;
    String cargueInicial = Generales.EMPTYSTRING;
    String tareasAbiertas = Generales.EMPTYSTRING;
    String tareasAtrasadas = Generales.EMPTYSTRING;
    String tareasCerradas = Generales.EMPTYSTRING;
    String tareasPendientes = Generales.EMPTYSTRING;
    String tareasObservadas = Generales.EMPTYSTRING;
    String contadorEncargado = Generales.EMPTYSTRING;
    String logoEmpresa = Generales.EMPTYSTRING;
    String nombreContrato = Generales.EMPTYSTRING;
    String idContrato = Generales.EMPTYSTRING;
    String estadoContrato = Generales.EMPTYSTRING;
    String valorPagado = Generales.EMPTYSTRING;
    String porcentajeContador = Generales.EMPTYSTRING;
    String fechaUltimoPago = Generales.EMPTYSTRING;
    String estadoTareasEmpresa = Generales.EMPTYSTRING;
    String gerente = Generales.EMPTYSTRING;
    String implementacion = Generales.EMPTYSTRING;
    String valorContador = Generales.EMPTYSTRING;
    String implementado = Generales.EMPTYSTRING;
    String userSiigo = Generales.EMPTYSTRING;
    String passwordSiigo = Generales.EMPTYSTRING;
    String implementadoSiigo = Generales.EMPTYSTRING;
    String estadoPago = Generales.EMPTYSTRING;
    String idGerente = Generales.EMPTYSTRING;
    String celularGerente = Generales.EMPTYSTRING;
    String celularContador = Generales.EMPTYSTRING;
    String correoGerente = Generales.EMPTYSTRING;
    String correoContador = Generales.EMPTYSTRING;
    String nombreGerente = Generales.EMPTYSTRING;
    String nombreContador = Generales.EMPTYSTRING;
    String idContadorAsignado = Generales.EMPTYSTRING;
    String estadoRegistro = Generales.EMPTYSTRING;
    String eficienciaEmpresa = Generales.EMPTYSTRING;
    ArrayList<EjecucionTareaDTO> tareas = new ArrayList<>();
    ArrayList<DatosUsuarioDTO> usuarios = new ArrayList<>();
    String avance = Generales.EMPTYSTRING;
    String razonSocial = Generales.EMPTYSTRING;
    String estadoAnterior = Generales.EMPTYSTRING;
    String fechaFacturacion = Generales.EMPTYSTRING;
    String nombreVendedor = Generales.EMPTYSTRING;
    String idVendedor = Generales.EMPTYSTRING;

    /**
     *
     * @return
     */
    public String getCantidad() {
        return cantidad;
    }

    /**
     *
     * @param cantidad
     */
    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    /**
     *
     * @return
     */
    public String getCodigoVerificacion() {
        return codigoVerificacion;
    }

    /**
     *
     * @param codigoVerificacion
     */
    public void setCodigoVerificacion(String codigoVerificacion) {
        this.codigoVerificacion = codigoVerificacion;
    }

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @return
     */
    public String getNombreComercial() {
        return nombreComercial;
    }

    /**
     *
     * @param nombreComercial
     */
    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    /**
     *
     * @return
     */
    public String getPrimerNombre() {
        return primerNombre;
    }

    /**
     *
     * @param primerNombre
     */
    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    /**
     *
     * @return
     */
    public String getOtrosNombre() {
        return otrosNombre;
    }

    /**
     *
     * @param otrosNombre
     */
    public void setOtrosNombre(String otrosNombre) {
        this.otrosNombre = otrosNombre;
    }

    /**
     *
     * @return
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     *
     * @param primerApellido
     */
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    /**
     *
     * @return
     */
    public String getSegundoApellido() {
        return segundoApellido;
    }

    /**
     *
     * @param segundoApellido
     */
    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    /**
     *
     * @return
     */
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
     *
     * @param numeroContrato
     */
    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    /**
     *
     * @return
     */
    public String getFechaInicial() {
        return fechaInicial;
    }

    /**
     *
     * @param fechaInicial
     */
    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    /**
     *
     * @return
     */
    public String getFechaFinal() {
        return fechaFinal;
    }

    /**
     *
     * @param fechaFinal
     */
    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    /**
     *
     * @return
     */
    public String getCelular() {
        return celular;
    }

    /**
     *
     * @param celular
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     *
     * @return
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     *
     * @param direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     *
     * @return
     */
    public String getCorreo() {
        return correo;
    }

    /**
     *
     * @param correo
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     *
     * @return
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     *
     * @param telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     *
     * @return
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * @return
     */
    public String getNit() {
        return nit;
    }

    /**
     *
     * @param nit
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     *
     * @return
     */
    public String getIdContador() {
        return idContador;
    }

    /**
     *
     * @param idContador
     */
    public void setIdContador(String idContador) {
        this.idContador = idContador;
    }

    /**
     *
     * @return
     */
    public String getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     *
     * @param idTipoDocumento
     */
    public void setIdTipoDocumento(String idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    /**
     *
     * @return
     */
    public String getIdTipoRegimen() {
        return idTipoRegimen;
    }

    /**
     *
     * @param idTipoRegimen
     */
    public void setIdTipoRegimen(String idTipoRegimen) {
        this.idTipoRegimen = idTipoRegimen;
    }

    /**
     *
     * @return
     */
    public String getDocumento() {
        return documento;
    }

    /**
     *
     * @param documento
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     *
     * @return
     */
    public String getRepresentanteLegal() {
        return representanteLegal;
    }

    /**
     *
     * @param representanteLegal
     */
    public void setRepresentanteLegal(String representanteLegal) {
        this.representanteLegal = representanteLegal;
    }

    /**
     *
     * @return
     */
    public String getIdMunicipio() {
        return idMunicipio;
    }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(String idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    /**
     *
     * @return
     */
    public String getIdDepartamento() {
        return idDepartamento;
    }

    /**
     *
     * @param idDepartamento
     */
    public void setIdDepartamento(String idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    /**
     *
     * @return
     */
    public String getPersonaJuridica() {
        return personaJuridica;
    }

    /**
     *
     * @param personaJuridica
     */
    public void setPersonaJuridica(String personaJuridica) {
        this.personaJuridica = personaJuridica;
    }

    /**
     *
     * @return
     */
    public String getPersonaNatural() {
        return personaNatural;
    }

    /**
     *
     * @param personaNatural
     */
    public void setPersonaNatural(String personaNatural) {
        this.personaNatural = personaNatural;
    }

    /**
     *
     * @return
     */
    public String getIdTipoEmpresa() {
        return idTipoEmpresa;
    }

    /**
     *
     * @param idTipoEmpresa
     */
    public void setIdTipoEmpresa(String idTipoEmpresa) {
        this.idTipoEmpresa = idTipoEmpresa;
    }

    /**
     *
     * @return
     */
    public String getRegistradoPor() {
        return registradoPor;
    }

    /**
     *
     * @param registradoPor
     */
    public void setRegistradoPor(String registradoPor) {
        this.registradoPor = registradoPor;
    }

    /**
     *
     * @return
     */
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     *
     * @param fechaRegistro
     */
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    /**
     *
     * @return
     */
    public String getDialaboralSabado() {
        return dialaboralSabado;
    }

    /**
     *
     * @param dialaboralSabado
     */
    public void setDialaboralSabado(String dialaboralSabado) {
        this.dialaboralSabado = dialaboralSabado;
    }

    /**
     *
     * @return
     */
    public String getDialaboralDomingo() {
        return dialaboralDomingo;
    }

    /**
     *
     * @param dialaboralDomingo
     */
    public void setDialaboralDomingo(String dialaboralDomingo) {
        this.dialaboralDomingo = dialaboralDomingo;
    }

    /**
     *
     * @return
     */
    public String getDialaboralFestivos() {
        return dialaboralFestivos;
    }

    /**
     *
     * @param dialaboralFestivos
     */
    public void setDialaboralFestivos(String dialaboralFestivos) {
        this.dialaboralFestivos = dialaboralFestivos;
    }

    /**
     *
     * @return
     */
    public String getCalificacionContador() {
        return calificacionContador;
    }

    /**
     *
     * @param calificacionContador
     */
    public void setCalificacionContador(String calificacionContador) {
        this.calificacionContador = calificacionContador;
    }

    /**
     *
     * @return
     */
    public String getCargueInicial() {
        return cargueInicial;
    }

    /**
     *
     * @param cargueInicial
     */
    public void setCargueInicial(String cargueInicial) {
        this.cargueInicial = cargueInicial;
    }

    /**
     *
     * @return
     */
    public String getMunicipio() {
        return municipio;
    }

    /**
     *
     * @param municipio
     */
    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    /**
     *
     * @return
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     *
     * @param departamento
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     *
     * @return
     */
    public String getTareasAbiertas() {
        return tareasAbiertas;
    }

    /**
     *
     * @param tareasAbiertas
     */
    public void setTareasAbiertas(String tareasAbiertas) {
        this.tareasAbiertas = tareasAbiertas;
    }

    /**
     *
     * @return
     */
    public String getTareasAtrasadas() {
        return tareasAtrasadas;
    }

    /**
     *
     * @param tareasAtrasadas
     */
    public void setTareasAtrasadas(String tareasAtrasadas) {
        this.tareasAtrasadas = tareasAtrasadas;
    }

    /**
     *
     * @return
     */
    public String getContadorEncargado() {
        return contadorEncargado;
    }

    /**
     *
     * @param contadorEncargado
     */
    public void setContadorEncargado(String contadorEncargado) {
        this.contadorEncargado = contadorEncargado;
    }

    /**
     *
     * @return
     */
    public String getLogoEmpresa() {
        return logoEmpresa;
    }

    /**
     *
     * @param logoEmpresa
     */
    public void setLogoEmpresa(String logoEmpresa) {
        this.logoEmpresa = logoEmpresa;
    }

    /**
     *
     * @return
     */
    public String getTareasCerradas() {
        return tareasCerradas;
    }

    /**
     *
     * @param tareasCerradas
     */
    public void setTareasCerradas(String tareasCerradas) {
        this.tareasCerradas = tareasCerradas;
    }

    /**
     *
     * @return
     */
    public String getNombreContrato() {
        return nombreContrato;
    }

    /**
     *
     * @param nombreContrato
     */
    public void setNombreContrato(String nombreContrato) {
        this.nombreContrato = nombreContrato;
    }

    /**
     *
     * @return
     */
    public String getIdContrato() {
        return idContrato;
    }

    /**
     *
     * @param idContrato
     */
    public void setIdContrato(String idContrato) {
        this.idContrato = idContrato;
    }

    /**
     *
     * @return
     */
    public String getEstadoContrato() {
        return estadoContrato;
    }

    /**
     *
     * @param estadoContrato
     */
    public void setEstadoContrato(String estadoContrato) {
        this.estadoContrato = estadoContrato;
    }

    /**
     *
     * @return
     */
    public String getValorPagado() {
        return valorPagado;
    }

    /**
     *
     * @param valorPagado
     */
    public void setValorPagado(String valorPagado) {
        this.valorPagado = valorPagado;
    }

    /**
     *
     * @return
     */
    public String getPorcentajeContador() {
        return porcentajeContador;
    }

    /**
     *
     * @param porcentajeContador
     */
    public void setPorcentajeContador(String porcentajeContador) {
        this.porcentajeContador = porcentajeContador;
    }

    /**
     *
     * @return
     */
    public String getFechaUltimoPago() {
        return fechaUltimoPago;
    }

    /**
     *
     * @param fechaUltimoPago
     */
    public void setFechaUltimoPago(String fechaUltimoPago) {
        this.fechaUltimoPago = fechaUltimoPago;
    }

    /**
     *
     * @return
     */
    public String getEstadoTareasEmpresa() {
        return estadoTareasEmpresa;
    }

    /**
     *
     * @param estadoTareasEmpresa
     */
    public void setEstadoTareasEmpresa(String estadoTareasEmpresa) {
        this.estadoTareasEmpresa = estadoTareasEmpresa;
    }

    /**
     *
     * @return
     */
    public String getGerente() {
        return gerente;
    }

    /**
     *
     * @param gerente
     */
    public void setGerente(String gerente) {
        this.gerente = gerente;
    }

    /**
     *
     * @return
     */
    public String getImplementacion() {
        return implementacion;
    }

    /**
     *
     * @param implementacion
     */
    public void setImplementacion(String implementacion) {
        this.implementacion = implementacion;
    }

    /**
     *
     * @return
     */
    public String getImplementadoSiigo() {
        return implementadoSiigo;
    }

    /**
     *
     * @param implementadoSiigo
     */
    public void setImplementadoSiigo(String implementadoSiigo) {
        this.implementadoSiigo = implementadoSiigo;
    }

    /**
     *
     * @return
     */
    public String getTareasPendientes() {
        return tareasPendientes;
    }

    /**
     *
     * @param tareasPendientes
     */
    public void setTareasPendientes(String tareasPendientes) {
        this.tareasPendientes = tareasPendientes;
    }

    /**
     *
     * @return
     */
    public String getTareasObservadas() {
        return tareasObservadas;
    }

    /**
     *
     * @param tareasObservadas
     */
    public void setTareasObservadas(String tareasObservadas) {
        this.tareasObservadas = tareasObservadas;
    }

    /**
     *
     * @return
     */
    public String getValorContador() {
        return valorContador;
    }

    /**
     *
     * @param valorContador
     */
    public void setValorContador(String valorContador) {
        this.valorContador = valorContador;
    }

    /**
     *
     * @return
     */
    public String getImplementado() {
        return implementado;
    }

    /**
     *
     * @param implementado
     */
    public void setImplementado(String implementado) {
        this.implementado = implementado;
    }

    /**
     *
     * @return
     */
    public String getUserSiigo() {
        return userSiigo;
    }

    /**
     *
     * @param userSiigo
     */
    public void setUserSiigo(String userSiigo) {
        this.userSiigo = userSiigo;
    }

    /**
     *
     * @return
     */
    public String getPasswordSiigo() {
        return passwordSiigo;
    }

    /**
     *
     * @param passwordSiigo
     */
    public void setPasswordSiigo(String passwordSiigo) {
        this.passwordSiigo = passwordSiigo;
    }

    /**
     *
     * @return
     */
    public String getEstadoPago() {
        return estadoPago;
    }

    /**
     *
     * @param estadoPago
     */
    public void setEstadoPago(String estadoPago) {
        this.estadoPago = estadoPago;
    }

    /**
     *
     * @return
     */
    public String getIdGerente() {
        return idGerente;
    }

    /**
     *
     * @param idGerente
     */
    public void setIdGerente(String idGerente) {
        this.idGerente = idGerente;
    }

    /**
     *
     * @return
     */
    public String getIdContadorAsignado() {
        return idContadorAsignado;
    }

    /**
     *
     * @param idContadorAsignado
     */
    public void setIdContadorAsignado(String idContadorAsignado) {
        this.idContadorAsignado = idContadorAsignado;
    }

    /**
     *
     * @return
     */
    public String getEstadoRegistro() {
        return estadoRegistro;
    }

    /**
     *
     * @param estadoRegistro
     */
    public void setEstadoRegistro(String estadoRegistro) {
        this.estadoRegistro = estadoRegistro;
    }

    /**
     *
     * @return
     */
    public ArrayList<EjecucionTareaDTO> getTareas() {
        return tareas;
    }

    /**
     *
     * @param tareas
     */
    public void setTareas(ArrayList<EjecucionTareaDTO> tareas) {
        this.tareas = tareas;
    }

    /**
     *
     * @return
     */
    public String getCelularGerente() {
        return celularGerente;
    }

    /**
     *
     * @param celularGerente
     */
    public void setCelularGerente(String celularGerente) {
        this.celularGerente = celularGerente;
    }

    /**
     *
     * @return
     */
    public String getCelularContador() {
        return celularContador;
    }

    /**
     *
     * @param celularContador
     */
    public void setCelularContador(String celularContador) {
        this.celularContador = celularContador;
    }

    /**
     *
     * @return
     */
    public String getCorreoGerente() {
        return correoGerente;
    }

    /**
     *
     * @param correoGerente
     */
    public void setCorreoGerente(String correoGerente) {
        this.correoGerente = correoGerente;
    }

    /**
     *
     * @return
     */
    public String getCorreoContador() {
        return correoContador;
    }

    /**
     *
     * @param correoContador
     */
    public void setCorreoContador(String correoContador) {
        this.correoContador = correoContador;
    }

    /**
     *
     * @return
     */
    public String getNombreGerente() {
        return nombreGerente;
    }

    /**
     *
     * @param nombreGerente
     */
    public void setNombreGerente(String nombreGerente) {
        this.nombreGerente = nombreGerente;
    }

    /**
     *
     * @return
     */
    public String getNombreContador() {
        return nombreContador;
    }

    /**
     *
     * @param nombreContador
     */
    public void setNombreContador(String nombreContador) {
        this.nombreContador = nombreContador;
    }

    /**
     *
     * @return
     */
    public ArrayList<DatosUsuarioDTO> getUsuarios() {
        return usuarios;
    }

    /**
     *
     * @param usuarios
     */
    public void setUsuarios(ArrayList<DatosUsuarioDTO> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     *
     * @return
     */
    public String getEficienciaEmpresa() {
        return eficienciaEmpresa;
    }

    /**
     *
     * @param eficienciaEmpresa
     */
    public void setEficienciaEmpresa(String eficienciaEmpresa) {
        this.eficienciaEmpresa = eficienciaEmpresa;
    }

    /**
     *
     * @return
     */
    public String getAvance() {
        return avance;
    }

    /**
     *
     * @param avance
     */
    public void setAvance(String avance) {
        this.avance = avance;
    }

    /**
     *
     * @return
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     *
     * @param razonSocial
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    /**
     *
     * @return
     */
    public String getEstadoAnterior() {
        return estadoAnterior;
    }

    /**
     *
     * @param estadoAnterior
     */
    public void setEstadoAnterior(String estadoAnterior) {
        this.estadoAnterior = estadoAnterior;
    }

    /**
     *
     * @return
     */
    public String getFechaFacturacion() {
        return fechaFacturacion;
    }

    /**
     *
     * @param fechaFacturacion
     */
    public void setFechaFacturacion(String fechaFacturacion) {
        this.fechaFacturacion = fechaFacturacion;
    }

    /**
     *
     * @return
     */
    public String getNombreVendedor() {
        return nombreVendedor;
    }

    /**
     *
     * @param nombreVendedor
     */
    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    /**
     *
     * @return
     */
    public String getIdVendedor() {
        return idVendedor;
    }

    /**
     *
     * @param idVendedor
     */
    public void setIdVendedor(String idVendedor) {
        this.idVendedor = idVendedor;
    }

    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
