/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Administrador
 */
public class ContadorDTO implements Serializable {

    String contadorEmpresa = Generales.ESTADO_INACTIVO;
    String idContador = Generales.EMPTYSTRING;
    String idEmpresaContador = Generales.EMPTYSTRING;
    String nombreComercial = Generales.EMPTYSTRING;
    String primerNombre = Generales.EMPTYSTRING;
    String otrosNombres = Generales.EMPTYSTRING;
    String primerApellido = Generales.EMPTYSTRING;
    String segundoApellido = Generales.EMPTYSTRING;
    String direccion = Generales.EMPTYSTRING;
    String telefono = Generales.EMPTYSTRING;
    String celular = Generales.EMPTYSTRING;
    String calificacion = Generales.EMPTYSTRING;
    String correo = Generales.EMPTYSTRING;
    String idTipoContador = Generales.EMPTYSTRING;
    String idTipoDocumento = Generales.EMPTYSTRING;
    String documento = Generales.EMPTYSTRING;
    String rut = Generales.EMPTYSTRING;
    String idMunicipio = Generales.EMPTYSTRING;
    String idDepartamento = Generales.EMPTYSTRING;
    String municipio = Generales.EMPTYSTRING;
    String departamento = Generales.EMPTYSTRING;
    String registradoPor = Generales.EMPTYSTRING;
    String fechaRegistro = Generales.EMPTYSTRING;
    String estado = Generales.EMPTYSTRING;
    String razonSocial = Generales.EMPTYSTRING;
    String nit = Generales.EMPTYSTRING;
    String codigoVerificacion = Generales.EMPTYSTRING;
    String tipoRegimen = Generales.EMPTYSTRING;
    String tarjetaPro = Generales.EMPTYSTRING;
    String nombreGeneral = Generales.EMPTYSTRING;
    String fechaFinalizacion = Generales.EMPTYSTRING;
    String idEmpresa = Generales.EMPTYSTRING;
    String idUsuario = Generales.EMPTYSTRING;
    String cuentaBancaria = Generales.EMPTYSTRING;
    String correoUsuario = Generales.EMPTYSTRING;
    String tipoCuentaBancaria = Generales.EMPTYSTRING;
    String idBanco = Generales.EMPTYSTRING;
    String nEmpresas = Generales.EMPTYSTRING;
    String imgPerfil = Generales.EMPTYSTRING;
    ArrayList<EmpresaDTO> empresa = null;
    String idGerenteOperaciones = Generales.EMPTYSTRING;
    String nombreGerenteOperaciones = Generales.EMPTYSTRING;

    /**
     *
     * @return
     */
    public String getCalificacion() {
        return calificacion;
    }

    /**
     *
     * @param calificacion
     */
    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    /**
     *
     * @return
     */
    public String getContadorEmpresa() {
        return contadorEmpresa;
    }

    /**
     *
     * @param contadorEmpresa
     */
    public void setContadorEmpresa(String contadorEmpresa) {
        this.contadorEmpresa = contadorEmpresa;
    }

    /**
     *
     * @return
     */
    public String getIdContador() {
        return idContador;
    }

    /**
     *
     * @param idContador
     */
    public void setIdContador(String idContador) {
        this.idContador = idContador;
    }

    /**
     *
     * @return
     */
    public String getIdEmpresaContador() {
        return idEmpresaContador;
    }

    /**
     *
     * @param idEmpresaContador
     */
    public void setIdEmpresaContador(String idEmpresaContador) {
        this.idEmpresaContador = idEmpresaContador;
    }

    /**
     *
     * @return
     */
    public String getNombreComercial() {
        return nombreComercial;
    }

    /**
     *
     * @param nombreComercial
     */
    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    /**
     *
     * @return
     */
    public String getPrimerNombre() {
        return primerNombre;
    }

    /**
     *
     * @param primerNombre
     */
    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    /**
     *
     * @return
     */
    public String getOtrosNombres() {
        return otrosNombres;
    }

    /**
     *
     * @param otrosNombres
     */
    public void setOtrosNombres(String otrosNombres) {
        this.otrosNombres = otrosNombres;
    }

    /**
     *
     * @return
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     *
     * @param primerApellido
     */
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    /**
     *
     * @return
     */
    public String getSegundoApellido() {
        return segundoApellido;
    }

    /**
     *
     * @param segundoApellido
     */
    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    /**
     *
     * @return
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     *
     * @param direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     *
     * @return
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     *
     * @param telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     *
     * @return
     */
    public String getCelular() {
        return celular;
    }

    /**
     *
     * @param celular
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     *
     * @return
     */
    public String getCorreo() {
        return correo;
    }

    /**
     *
     * @param correo
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     *
     * @return
     */
    public String getIdTipoContador() {
        return idTipoContador;
    }

    /**
     *
     * @param idTipoContador
     */
    public void setIdTipoContador(String idTipoContador) {
        this.idTipoContador = idTipoContador;
    }
    /**
     *
     * @return
     */
    public String getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     *
     * @param idTipoDocumento
     */
    public void setIdTipoDocumento(String idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    /**
     *
     * @return
     */
    public String getDocumento() {
        return documento;
    }

    /**
     *
     * @param documento
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     *
     * @return
     */
    public String getRut() {
        return rut;
    }

    /**
     *
     * @param rut
     */
    public void setRut(String rut) {
        this.rut = rut;
    }

    /**
     *
     * @return
     */
    public String getIdMunicipio() {
        return idMunicipio;
    }

    /**
     *
     * @param idMunicipio
     */
    public void setIdMunicipio(String idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    /**
     *
     * @return
     */
    public String getIdDepartamento() {
        return idDepartamento;
    }

    /**
     *
     * @param idDepartamento
     */
    public void setIdDepartamento(String idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    /**
     *
     * @return
     */
    public String getRegistradoPor() {
        return registradoPor;
    }

    /**
     *
     * @param registradoPor
     */
    public void setRegistradoPor(String registradoPor) {
        this.registradoPor = registradoPor;
    }

    /**
     *
     * @return
     */
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     *
     * @param fechaRegistro
     */
    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    /**
     *
     * @return
     */
    public String getEstado() {
        return estado;
    }

    /**
     *
     * @param estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     *
     * @return
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     *
     * @param razonSocial
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    /**
     *
     * @return
     */
    public String getNit() {
        return nit;
    }

    /**
     *
     * @param nit
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     *
     * @return
     */
    public String getTipoRegimen() {
        return tipoRegimen;
    }

    /**
     *
     * @param tipoRegimen
     */
    public void setTipoRegimen(String tipoRegimen) {
        this.tipoRegimen = tipoRegimen;
    }

    /**
     *
     * @return
     */
    public String getTarjetaPro() {
        return tarjetaPro;
    }

    /**
     *
     * @param tarjetaPro
     */
    public void setTarjetaPro(String tarjetaPro) {
        this.tarjetaPro = tarjetaPro;
    }

    /**
     *
     * @return
     */
    public String getCodigoVerificacion() {
        return codigoVerificacion;
    }

    /**
     *
     * @param codigoVerificacion
     */
    public void setCodigoVerificacion(String codigoVerificacion) {
        this.codigoVerificacion = codigoVerificacion;
    }

    /**
     *
     * @return
     */
    public String getMunicipio() {
        return municipio;
    }

    /**
     *
     * @param municipio
     */
    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    /**
     *
     * @return
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     *
     * @param departamento
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     *
     * @return
     */
    public String getNombreGeneral() {
        return nombreGeneral;
    }

    /**
     *
     * @param nombreGeneral
     */
    public void setNombreGeneral(String nombreGeneral) {
        this.nombreGeneral = nombreGeneral;
    }

    /**
     *
     * @return
     */
    public String getFechaFinalizacion() {
        return fechaFinalizacion;
    }

    /**
     *
     * @param fechaFinalizacion
     */
    public void setFechaFinalizacion(String fechaFinalizacion) {
        this.fechaFinalizacion = fechaFinalizacion;
    }

    /**
     *
     * @return
     */
    public String getIdEmpresa() {
        return idEmpresa;
    }

    /**
     *
     * @param idEmpresa
     */
    public void setIdEmpresa(String idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     *
     * @return
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     */
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     */
    public String getCuentaBancaria() {
        return cuentaBancaria;
    }

    /**
     *
     * @param cuentaBancaria
     */
    public void setCuentaBancaria(String cuentaBancaria) {
        this.cuentaBancaria = cuentaBancaria;
    }

    /**
     *
     * @return
     */
    public String getCorreoUsuario() {
        return correoUsuario;
    }

    /**
     *
     * @param correoUsuario
     */
    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }

    /**
     *
     * @return
     */
    public String getTipoCuentaBancaria() {
        return tipoCuentaBancaria;
    }

    /**
     *
     * @param tipoCuentaBancaria
     */
    public void setTipoCuentaBancaria(String tipoCuentaBancaria) {
        this.tipoCuentaBancaria = tipoCuentaBancaria;
    }

    /**
     *
     * @return
     */
    public String getIdBanco() {
        return idBanco;
    }

    /**
     *
     * @param idBanco
     */
    public void setIdBanco(String idBanco) {
        this.idBanco = idBanco;
    }

    /**
     *
     * @return
     */
    public String getnEmpresas() {
        return nEmpresas;
    }

    /**
     *
     * @param nEmpresas
     */
    public void setnEmpresas(String nEmpresas) {
        this.nEmpresas = nEmpresas;
    }

    /**
     *
     * @return
     */
    public ArrayList<EmpresaDTO> getEmpresa() {
        return empresa;
    }

    /**
     *
     * @param empresa
     */
    public void setEmpresa(ArrayList<EmpresaDTO> empresa) {
        this.empresa = empresa;
    }

    /**
     *
     * @return
     */
    public String getImgPerfil() {
        return imgPerfil;
    }

    /**
     *
     * @param imgPerfil
     */
    public void setImgPerfil(String imgPerfil) {
        this.imgPerfil = imgPerfil;
    }

    public String getIdGerenteOperaciones() {
        return idGerenteOperaciones;
    }

    public void setIdGerenteOperaciones(String idGerenteOperaciones) {
        this.idGerenteOperaciones = idGerenteOperaciones;
    }

    public String getNombreGerenteOperaciones() {
        return nombreGerenteOperaciones;
    }

    public void setNombreGerenteOperaciones(String nombreGerenteOperaciones) {
        this.nombreGerenteOperaciones = nombreGerenteOperaciones;
    }

    
    
    
    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }
}
