/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.DTOs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuscuentas.manejotareas.common.util.Generales;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author carlos
 */
public class RespuestaVistaDTO implements Serializable {

    String condicion = Generales.EMPTYSTRING;
    String mensaje = Generales.EMPTYSTRING;
    String tabla = Generales.EMPTYSTRING;
    boolean registro = false;
    ArrayList mensajes = new ArrayList();

    String idResgistrado = Generales.EMPTYSTRING;

    /**
     *
     * @return
     */
    public String getIdResgistrado() {
        return idResgistrado;
    }

    /**
     *
     * @param idResgistrado
     */
    public void setIdResgistrado(String idResgistrado) {
        this.idResgistrado = idResgistrado;
    }

    /**
     *
     * @return
     */
    public String getCondicion() {
        return condicion;
    }

    /**
     *
     * @param condicion
     */
    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    /**
     *
     * @return
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     *
     * @param mensaje
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     *
     * @return
     */
    public ArrayList getMensajes() {
        return mensajes;
    }

    /**
     *
     * @param mensajes
     */
    public void setMensajes(ArrayList mensajes) {
        this.mensajes = mensajes;
    }

    /**
     *
     * @return
     */
    public String getTabla() {
        return tabla;
    }

    /**
     *
     * @param tabla
     */
    public void setTabla(String tabla) {
        this.tabla = tabla;
    }

    /**
     *
     * @return
     */
    public boolean isRegistro() {
        return registro;
    }

    /**
     *
     * @param registro
     */
    public void setRegistro(boolean registro) {
        this.registro = registro;
    }

    /**
     *
     * @return
     */
    public String toStringJson() {
        String dtoJsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            dtoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (Exception e) {
        }
        return dtoJsonString;
    }

}
