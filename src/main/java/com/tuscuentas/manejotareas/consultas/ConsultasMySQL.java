/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tuscuentas.manejotareas.consultas;

import com.tuscuentas.manejotareas.DTOs.ActividadPreDTO;
import com.tuscuentas.manejotareas.DTOs.ActividadPredeterminadaDTO;
import com.tuscuentas.manejotareas.DTOs.CheckListDTO;
import com.tuscuentas.manejotareas.DTOs.ContadorDTO;
import com.tuscuentas.manejotareas.DTOs.EjecucionProximaDTO;
import com.tuscuentas.manejotareas.DTOs.EjecucionTareaDTO;
import com.tuscuentas.manejotareas.DTOs.EmpresaDTO;
import com.tuscuentas.manejotareas.DTOs.LogActividadesDTO;
import com.tuscuentas.manejotareas.DTOs.RespuestaVistaDTO;
import com.tuscuentas.manejotareas.DTOs.TareaPredeterminadaDTO;
import com.tuscuentas.manejotareas.DTOs.UsuarioDTO;
import com.tuscuentas.manejotareas.common.util.AsignaAtributoStatement;
import com.tuscuentas.manejotareas.common.util.Generales;
import com.tuscuentas.manejotareas.conexion.ConexionMySQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author dario
 */
public class ConsultasMySQL {

    // Instancias la clase que hemos creado anteriormente
    private ConexionMySQL SQL = new ConexionMySQL();
// Llamas al método que tiene la clase y te devuelve una conexión
    private final Connection conn;
// Query que usarás para hacer lo que necesites
    private String sSQL = "";
    StringBuilder cadSQL = null;

    PreparedStatement ps = null;
    ResultSet rs = null;

    public ConsultasMySQL() throws SQLException {
        this.conn = SQL.conectarMySQL();
    }

// Query
//sSQL =  "INSERT INTO USERS (first_name, last_name) VALUES (?, ?)";
// PreparedStatement
//PreparedStatement pstm = conn.prepareStatement(sSQL);
    public ArrayList<UsuarioDTO> listarUsuarios() throws SQLException {

        ArrayList<UsuarioDTO> listado = null;
        UsuarioDTO datosUsuario = null;
        cadSQL = new StringBuilder();

        cadSQL.append(" SELECT usua_id,usua_primernombre,usua_otrosnombres,usua_primerapellido,usua_segundoapellido,municipio.depa_id,usua_eficacia,");
        cadSQL.append(" usua_correo,usuario.tido_id,usua_documento,usua_celular,usuario.muni_id,usua_direccion,empr_id, usua_fecharegistro,");
        cadSQL.append(" usuario.coju_id,usuario.tius_id,coju_ppal,usua_imgperfil,usua_usuario,usua_password,usua_estado,usua_dv,coju.coju_estado, ");
        cadSQL.append(" tipo_usuario.tius_descripcion, departamento.depa_nombre, municipio.muni_nombre FROM usuario ");
        cadSQL.append(" LEFT JOIN municipio ON municipio.muni_id = usuario.muni_id");
        cadSQL.append(" LEFT JOIN departamento ON departamento.depa_id = municipio.depa_id ");
        cadSQL.append(" INNER JOIN tipo_usuario ON tipo_usuario.tius_id = usuario.tius_id");
        cadSQL.append(" LEFT JOIN contador_juridico coju ON coju.coju_id = usuario.coju_id");
        cadSQL.append(" WHERE NOT usuario.tius_id = '1' ");

        ps = conn.prepareStatement(cadSQL.toString());

        rs = ps.executeQuery();
        listado = new ArrayList<>();
        while (rs.next()) {
            datosUsuario = new UsuarioDTO();
            datosUsuario.setId(rs.getString("usua_id"));
            datosUsuario.setPrimerNombre(rs.getString("usua_primernombre"));
            datosUsuario.setOtrosNombres(rs.getString("usua_otrosnombres"));
            datosUsuario.setPrimerApellido(rs.getString("usua_primerapellido"));
            datosUsuario.setSegundoApellido(rs.getString("usua_segundoapellido"));
            datosUsuario.setCorreo(rs.getString("usua_correo"));
            datosUsuario.setIdTipoDocumento(rs.getString("tido_id"));
            datosUsuario.setDocumento(rs.getString("usua_documento"));
            datosUsuario.setCelular(rs.getString("usua_celular"));
            datosUsuario.setIdMunicipio(rs.getString("muni_id"));
            datosUsuario.setDireccion(rs.getString("usua_direccion"));
            datosUsuario.setIdEmpresa(rs.getString("empr_id"));
            datosUsuario.setIdContadorJuridico(rs.getString("coju_id"));
            datosUsuario.setIdTipoUsuario(rs.getString("tius_id"));
            datosUsuario.setTipoUsuario(rs.getString("tipo_usuario.tius_descripcion"));
            datosUsuario.setContadorPrincipal(rs.getString("coju_ppal"));
            datosUsuario.setImagenPerfil(rs.getString("usua_imgperfil"));
            datosUsuario.setUsuario(rs.getString("usua_usuario"));
            datosUsuario.setClave(rs.getString("usua_password"));
            datosUsuario.setEstado(rs.getString("usua_estado"));
            datosUsuario.setFechaRegistro(rs.getString("usua_fecharegistro"));
            datosUsuario.setDepartamento(rs.getString("departamento.depa_nombre"));
            datosUsuario.setMunicipio(rs.getString("municipio.muni_nombre"));
            datosUsuario.setEficacia(rs.getString("usua_eficacia"));
            datosUsuario.setDv(rs.getString("usua_dv"));
            datosUsuario.setEstadoContador(rs.getString("coju.coju_estado"));
            listado.add(datosUsuario);
        }
        ps.close();
        ps = null;
        return listado;
    }

    public RespuestaVistaDTO actualizarEstadoAtrasada() {
        RespuestaVistaDTO respuestaVista = null;
        StringBuilder cadSQL = null;
        int nRows;
        try {

            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            String fechaActual = formato.format(calendar.getTime());

            respuestaVista = new RespuestaVistaDTO();
            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE ejecucion_tarea ejta  SET ejta.ejta_estado=2, ejta_leido =1 ");
            cadSQL.append(" WHERE ejta.ejta_habil=1 and ejta.ejta_fechafinal < ? and ejta.ejta_estado != 3");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, fechaActual, ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                respuestaVista = new RespuestaVistaDTO();
                respuestaVista.setRegistro(true);
                respuestaVista.setMensaje("Se actualizó con exito : actualizarEstadoAtrasada");
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            e.printStackTrace();
            return respuestaVista;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return respuestaVista;
            }
        }
        return respuestaVista;
    }

    public RespuestaVistaDTO actualizarEstadoAbierto() {
        RespuestaVistaDTO respuestaVista = null;
        StringBuilder cadSQL = null;
        int nRows;
        try {

            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            String fechaActual = formato.format(calendar.getTime());

            respuestaVista = new RespuestaVistaDTO();
            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE ejecucion_tarea ejta  SET ejta.ejta_estado=1, ejta_leido ='0' ");
            cadSQL.append(" WHERE  ejta.ejta_fechainicial <= ? and ejta.ejta_fechafinal >= ? and ejta.ejta_estado =0");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, fechaActual, ps);
            AsignaAtributoStatement.setString(2, fechaActual, ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                respuestaVista = new RespuestaVistaDTO();
                respuestaVista.setRegistro(true);
                respuestaVista.setMensaje("Se actualizó con exito : actualizarEstadoAbierto");
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            e.printStackTrace();
            return respuestaVista;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return respuestaVista;
            }
        }
        return respuestaVista;
    }

    public ArrayList<EjecucionTareaDTO> listarActividadesAtrasadasPorUsuarioServletTareaProgramada(String idUsuario, String idEmpresa) {
        PreparedStatement ps = null;

        ArrayList<EjecucionTareaDTO> lista = null;
        EjecucionTareaDTO ejecucion = null;
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT DISTINCT ejta.ejta_codejecucion,ejta.ejta_id, ejta.taem_id,ejta.ejta_fechainicial,ejta.ejta_fechafinal, ");
            cadSQL.append(" taem.tapr_id,taem.taem_nombre,if(taem.taem_descripcion is null,dein.dein_descripcion,taem.taem_descripcion) as descripcionEjecucion,if(taem.taem_descripcion is null,dein.dein_nombretarea,taem.taem_nombre) as nombreTarea,ejta.ejta_descripcion,ejta.ejta_estadoprorroga,");
            cadSQL.append(" empr.empr_nombre,ejta.usua_id,ejta_leido,ejta.ejta_estado,ejta.ejta_orden,CONCAT_WS(' ',empr_nombre, empr_primernombre, emp_primerapellido,empr_nombrecomercial  ) as nombre, if(ejta.ejta_fechafinal >= now(),0,1) as estadoDias,ejta.ejta_dias,taem.empr_id  ");
            cadSQL.append(" from ejecucion_tarea ejta ");
            cadSQL.append(" inner join tarea_empresa taem on ejta.taem_id = taem.taem_id ");
            cadSQL.append(" inner join tarea_predeterminada tapr on tapr.tapr_id = taem.tapr_id ");
            cadSQL.append(" left JOIN proceso_empresa pr_empr ON pr_empr.empr_id = taem.empr_id ");
            cadSQL.append(" inner join empresa empr on empr.empr_id = taem.empr_id ");
            cadSQL.append(" LEFT JOIN descripcion_indicador dein on dein.ejta_id = ejta.ejta_id ");

            if (idEmpresa.equals("0")) {
                cadSQL.append("  WHERE tapr.tapr_id >2 AND empr.empr_estado <> 0 and ejta.usua_id =? and ejta.ejta_estado = ? and if(pr_empr.prem_estado is not null, pr_empr.prem_estado = '1',pr_empr.prem_estado is null ) AND ejta_habil = '1'  order by ejta.ejta_fechafinal desc");
                ps = conn.prepareStatement(cadSQL.toString());
                AsignaAtributoStatement.setString(1, idUsuario, ps);
                AsignaAtributoStatement.setString(2, "2", ps);
            } else {
                cadSQL.append("  WHERE tapr.tapr_id >2 AND empr.empr_estado <> 0 and ejta.usua_id =? and ejta.ejta_estado = ? and if(pr_empr.prem_estado is not null, pr_empr.prem_estado = '1',pr_empr.prem_estado is null ) and empr.empr_id=? AND ejta_habil = '1' order by ejta.ejta_fechafinal desc");
                ps = conn.prepareStatement(cadSQL.toString());
                AsignaAtributoStatement.setString(1, idUsuario, ps);
                AsignaAtributoStatement.setString(2, "2", ps);
                AsignaAtributoStatement.setString(3, idEmpresa, ps);
            }

            rs = ps.executeQuery();
            lista = new ArrayList<EjecucionTareaDTO>();
            while (rs.next()) {

                ejecucion = new EjecucionTareaDTO();
                ejecucion.setId(rs.getString("ejta.ejta_id"));
                ejecucion.setCodEjcucion(rs.getString("ejta.ejta_codejecucion"));
                ejecucion.setIdTareaEmpresa(rs.getString("ejta.taem_id"));
                ejecucion.setFechaInicioEjecucion(rs.getString("ejta.ejta_fechainicial"));
                ejecucion.setFechaFinEjecucion(rs.getString("ejta.ejta_fechafinal"));
                ejecucion.setIdTarea(rs.getString("taem.tapr_id"));
                ejecucion.setNombreTarea(rs.getString("nombreTarea"));
                ejecucion.setNombreActividad(rs.getString("ejta.ejta_descripcion"));
                ejecucion.setNombreEmpresa(rs.getString("nombre"));
                ejecucion.setEjecucionLeida(rs.getString("ejta_leido"));
                ejecucion.setEstado(rs.getString("ejta.ejta_estado"));
                ejecucion.setOrden(rs.getString("ejta.ejta_orden"));
                ejecucion.setEstadoProrroga(rs.getString("ejta.ejta_estadoprorroga"));
                ejecucion.setNombreComercialEmpresa(rs.getString("nombre"));
                ejecucion.setDescripcionTarea(rs.getString("descripcionEjecucion"));
                ejecucion.setEstadoDias(rs.getString("estadoDias"));
                ejecucion.setIdEmpresa(rs.getString("taem.empr_id"));
                ejecucion.setIdUsuario(rs.getString("ejta.usua_id"));
                ejecucion.setDias(rs.getString("ejta.ejta_dias"));
                lista.add(ejecucion);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return lista;
    }

    public EjecucionProximaDTO validarEjecucionProxima(String codigoEjecucion, String idTareaEmpresa) {
        PreparedStatement ps = null;

        EjecucionProximaDTO ejecucion = null;
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT ejta.ejta_id FROM ejecucion_tarea  ejta");
            cadSQL.append(" WHERE ejta.ejta_codejecucion = ? AND ejta.taem_id = ?  ");
            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, codigoEjecucion, ps);
            AsignaAtributoStatement.setString(2, idTareaEmpresa, ps);

            rs = ps.executeQuery();

            while (rs.next()) {
                ejecucion = new EjecucionProximaDTO();
                ejecucion.setIdEjecucionProxima(rs.getString("ejta_id"));
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return ejecucion;
    }

    public EjecucionTareaDTO consultarTareaEmpresaPorIdEjecucion(String idEjecucion) {
        PreparedStatement ps = null;

        EjecucionTareaDTO ejecucion = null;
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append("  SELECT ejta.taem_id, taem.empr_id, ejta.ejta_fechainicial, ejta.ejta_codejecucion FROM ejecucion_tarea ejta ");
            cadSQL.append("  INNER JOIN tarea_empresa taem ON taem.taem_id =  ejta.taem_id");
            cadSQL.append("   WHERE ejta.ejta_habil = 1 AND ejta_id = ? AND  ejta.taem_id = (SELECT ej.taem_id FROM ejecucion_tarea ej WHERE ejta_id = ? )  ORDER BY ejta_id DESC, ejta_fechainicial ASC  LIMIT 1");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idEjecucion, ps);
            AsignaAtributoStatement.setString(2, idEjecucion, ps);

            rs = ps.executeQuery();

            while (rs.next()) {
                ejecucion = new EjecucionTareaDTO();
                ejecucion.setIdTareaEmpresa(rs.getString("taem_id"));
                ejecucion.setIdEmpresa(rs.getString("taem.empr_id"));
                ejecucion.setFechaFinEjecucion(rs.getString("ejta.ejta_fechainicial"));
                ejecucion.setCodEjcucion(rs.getString("ejta.ejta_codejecucion"));

            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return ejecucion;
    }

    public EjecucionProximaDTO listarActividadPorEjecutar(String idTareaEmpresa) {
        PreparedStatement ps = null;

        EjecucionProximaDTO ejecucion = null;
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT ejpr_fechainicial, ejpr.ejpr_id, ejpr.taem_id,taem.empr_id, ejpr.cafe_id, ejpr.ejta_codejecucion FROM ejecucion_proxima ejpr");
            cadSQL.append(" INNER JOIN ejecucion_tarea ejta ON ejta.taem_id = ejpr.taem_id ");
            cadSQL.append(" INNER JOIN tarea_empresa taem ON taem.taem_id = ejta.taem_id");
            cadSQL.append(" WHERE ejpr.taem_id = ? AND taem.taem_estado = ?");

            ps = conn.prepareStatement(cadSQL.toString());

            AsignaAtributoStatement.setString(1, idTareaEmpresa, ps);
            AsignaAtributoStatement.setString(2, "1", ps);

            rs = ps.executeQuery();
            while (rs.next()) {
                ejecucion = new EjecucionProximaDTO();
                ejecucion.setFechaInicialProxima(rs.getString("ejpr_fechainicial"));
                ejecucion.setIdEjecucionProxima(rs.getString("ejpr.ejpr_id"));
                ejecucion.setIdTareaEmpresa(rs.getString("ejpr.taem_id"));
                ejecucion.setIdEmpresa(rs.getString("taem.empr_id"));
                ejecucion.setCalendarioFecha(rs.getString("ejpr.cafe_id"));
                ejecucion.setCodigoEjecucionProxima(rs.getString("ejpr.ejta_codejecucion"));
            }
            ps.close();

            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return ejecucion;
    }

    public ArrayList<TareaPredeterminadaDTO> listarTareaPredeterminadaPorIdTareaEmpresa(String idTareaEmpresa) {
        PreparedStatement ps = null;

        ArrayList<TareaPredeterminadaDTO> lista = null;
        TareaPredeterminadaDTO tarea = null;
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT taem.taem_id , taem.tapr_id , taem.empr_id, taem.cafe_id, taem.taem_id , taem.taem_impuesto,");
            cadSQL.append(" taem.taem_idEjecucion, taem.taem_pasoejecucion_estado, taem.taem_pasoejecucion_cargoempresa,");
            cadSQL.append(" taem.taem_pasoejecucion_diashabiles,taem.taem_pasoejecucion_diasprorroga, taem.taem_idRevision,");
            cadSQL.append(" taem.taem_pasorevision_estado, taem.taem_pasorevision_cargoempresa,taem.taem_pasorevision_diashabiles,");
            cadSQL.append(" taem.taem_pasorevision_diasprorroga,taem.taem_idAprobacion,taem.taem_pasoaprobacion_estado,");
            cadSQL.append(" taem.taem_pasoaprobacion_cargoempresa, taem.taem_pasoaprobacion_diashabiles,");
            cadSQL.append(" taem.taem_pasoaprobacion_diasprorroga,taem.taem_idRecepcion,taem.taem_pasorecepcion_estado,");
            cadSQL.append(" taem.taem_pasorecepcion_cargoempresa,taem.taem_pasorecepcion_diashabiles,");
            cadSQL.append(" taem.taem_pasorecepcion_diasprorroga,taem.taem_recurrente,taem.tire_id,taem.taem_nombre,taem.empr_id");
            cadSQL.append(" FROM tarea_empresa taem");
            cadSQL.append(" LEFT JOIN proceso_empresa prem ON prem.empr_id = taem.empr_id AND  prem.proc_id = taem.taem_proceso AND prem.prem_estado =1 ");
            cadSQL.append(" WHERE taem.taem_id = ?  AND taem.taem_estado = ?");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idTareaEmpresa, ps);
            AsignaAtributoStatement.setString(2, "1", ps);

            rs = ps.executeQuery();
            lista = new ArrayList();
            while (rs.next()) {
                tarea = new TareaPredeterminadaDTO();
                tarea.setId(rs.getString("taem.tapr_id"));
                tarea.setIdEmpresa(rs.getString("taem.empr_id"));
                tarea.setCalendarioFecha(rs.getString("taem.cafe_id"));
                tarea.setNombreTarea(rs.getString("taem.taem_nombre"));
                tarea.setIdTareaEmpresa(rs.getString("taem.taem_id"));
                tarea.setImpuesto(rs.getString("taem.taem_impuesto"));
                tarea.setIdPasoEjecucion(rs.getString("taem.taem_idEjecucion"));
                tarea.setEstadoPasoEjecucion(rs.getString("taem.taem_pasoejecucion_estado"));
                tarea.setCargoEjecucion(rs.getString("taem.taem_pasoejecucion_cargoempresa"));
                tarea.setDiasEjecucion(rs.getString("taem.taem_pasoejecucion_diashabiles"));
                tarea.setProrrogaEjecucion(rs.getString("taem.taem_pasoejecucion_diasprorroga"));
                tarea.setIdPasoRevision(rs.getString("taem.taem_idRevision"));
                tarea.setEstadoPasoRevision(rs.getString("taem.taem_pasorevision_estado"));
                tarea.setCargoRevision(rs.getString("taem.taem_pasorevision_cargoempresa"));
                tarea.setDiasRevision(rs.getString("taem.taem_pasorevision_diashabiles"));
                tarea.setProrrogaRevision(rs.getString("taem.taem_pasorevision_diashabiles"));
                tarea.setIdPasoAprobacion(rs.getString("taem.taem_idAprobacion"));
                tarea.setEstadoPasoAprobacion(rs.getString("taem.taem_pasoaprobacion_estado"));
                tarea.setCargoAprobacion(rs.getString("taem.taem_pasoaprobacion_cargoempresa"));
                tarea.setDiasAprobacion(rs.getString("taem.taem_pasoaprobacion_diashabiles"));
                tarea.setProrrogaAprobacion(rs.getString("taem.taem_pasoaprobacion_diasprorroga"));
                tarea.setIdPasoRecepcion(rs.getString("taem.taem_idRecepcion"));
                tarea.setEstadoPasoRecepcion(rs.getString("taem.taem_pasorecepcion_estado"));
                tarea.setCargoRecepcion(rs.getString("taem.taem_pasorecepcion_cargoempresa"));
                tarea.setDiasRecepcion(rs.getString("taem.taem_pasorecepcion_diashabiles"));
                tarea.setProrrogaRecepcion(rs.getString("taem.taem_pasorecepcion_diasprorroga"));
                lista.add(tarea);
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return lista;
    }

    public EmpresaDTO obtenerInfoEmpresaPorId(String id) {
        PreparedStatement ps = null;

        EmpresaDTO datosEmpresa = null;
        StringBuilder cadSQL = null;
        try {

            cadSQL = new StringBuilder();

            cadSQL.append(" SELECT empr.empr_id, empr.empr_nombre,empr.empr_direccion,empr.empr_correo,empr.empr_telefono,empr.empr_nit,empr.coju_id,empr.empr_celular,");
            cadSQL.append(" empr.empr_nombrecomercial,empr.empr_primernombre,empr.empr_otrosnombres,empr.emp_primerapellido,empr.empr_segundoapellido,");
            cadSQL.append(" ifnull(empr.empr_nombre,'')as nombre,");
            cadSQL.append(" ifnull(empr.empr_primernombre,'')as primernombre,");
            cadSQL.append(" ifnull(empr.empr_otrosnombres,'')as segundonombre,");
            cadSQL.append(" ifnull(empr.emp_primerapellido,'')as primerapellido,");
            cadSQL.append(" ifnull(empr.empr_segundoapellido,'')as segundoapellido,");
            cadSQL.append(" tire_id,empr.muni_id,empr.empr_personajuridica,empr.empr_personanatural,empr.empr_valorcontador,empr_implementacion,");
            cadSQL.append(" tiem_id,empr.empr_registradopor,empr.empr_fecharegistro,empr.empr_sabado,empr.empr_domingo,empr.empr_festivos,empr.empr_implementadosiigo,");
            cadSQL.append(" if(empr.empr_estado=1 AND empr.empr_implementadosiigo = 1,'Producción', ");
            cadSQL.append(" if(empr.empr_estado=2,'Pre-Producción',");
            cadSQL.append(" if(empr.empr_estado=1 AND empr.empr_implementacion = 1,'Implementación',");
            cadSQL.append(" if(empr.empr_estado=1 AND empr.empr_implementacion = 2,'Reimplementación','')))) as estadoEmpresa,");
            cadSQL.append(" municipio.depa_id,municipio.muni_nombre, empr.tido_id, empr.empr_dv,empr.empr_contrato,empr.empr_userSiigo, ");
            cadSQL.append(" empr.empr_passwordSiigo,empr.empr_estado,empr.empr_cargueinicial,gere.usua_id,conas.usua_id,empr.empr_implementacion, ");
            cadSQL.append(" conas.usua_correo,gere.usua_correo, CONCAT_WS(' ',conas.usua_primernombre , conas.usua_primerapellido) as nombreContador,");
            cadSQL.append(" CONCAT_WS(' ',gere.usua_primernombre , gere.usua_primerapellido) as nombreGerente, conas.usua_celular, gere.usua_celular,empr.empr_estadoanterior ");
            cadSQL.append(" FROM empresa empr");
            cadSQL.append(" INNER JOIN municipio ON municipio.muni_id = empr.muni_id");
            cadSQL.append(" INNER JOIN gerente_empresa ger ON ger.empr_id=empr.empr_id ");
            cadSQL.append(" INNER JOIN usuario gere ON gere.usua_id = ger.usua_id");
            cadSQL.append(" INNER JOIN contador_empresa coem ON coem.empr_id =empr.empr_id");
            cadSQL.append(" INNER JOIN usuario conas ON conas.usua_id = coem.usua_id");
            cadSQL.append(" WHERE empr.empr_id= ? and gere.tius_id=? and coem.caem_id=? ");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, id, ps);
            AsignaAtributoStatement.setString(2, "7", ps);
            AsignaAtributoStatement.setString(3, "12", ps);

            rs = ps.executeQuery();
            while (rs.next()) {
                datosEmpresa = new EmpresaDTO();
                datosEmpresa.setId(rs.getString("empr_id"));
                datosEmpresa.setNombre(rs.getString("nombre"));
                datosEmpresa.setDireccion(rs.getString("empr_direccion"));
                datosEmpresa.setCorreo(rs.getString("empr_correo"));
                datosEmpresa.setTelefono(rs.getString("empr_telefono"));
                datosEmpresa.setNit(rs.getString("empr_nit"));
                datosEmpresa.setIdContador(rs.getString("coju_id"));
                datosEmpresa.setCelular(rs.getString("empr_celular"));
                datosEmpresa.setNombreComercial(rs.getString("empr_nombrecomercial"));
                datosEmpresa.setPrimerNombre(rs.getString("primernombre"));
                datosEmpresa.setOtrosNombre(rs.getString("segundonombre"));
                datosEmpresa.setPrimerApellido(rs.getString("primerapellido"));
                datosEmpresa.setSegundoApellido(rs.getString("segundoapellido"));
                datosEmpresa.setIdTipoRegimen(rs.getString("tire_id"));
                datosEmpresa.setCodigoVerificacion(rs.getString("empr_dv"));
                datosEmpresa.setIdMunicipio(rs.getString("empr.muni_id"));
                datosEmpresa.setPersonaJuridica(rs.getString("empr_personajuridica"));
                datosEmpresa.setPersonaNatural(rs.getString("empr_personanatural"));
                datosEmpresa.setIdDepartamento(rs.getString("municipio.depa_id"));
                datosEmpresa.setIdTipoEmpresa(rs.getString("tiem_id"));
                datosEmpresa.setDialaboralSabado(rs.getString("empr_sabado"));
                datosEmpresa.setDialaboralDomingo(rs.getString("empr_domingo"));
                datosEmpresa.setDialaboralFestivos(rs.getString("empr_festivos"));
                datosEmpresa.setIdTipoDocumento(rs.getString("tido_id"));
                datosEmpresa.setFechaRegistro(rs.getString("empr_fecharegistro"));
                datosEmpresa.setEstadoContrato(rs.getString("empr_contrato"));
                datosEmpresa.setValorContador(rs.getString("empr_valorcontador"));
                datosEmpresa.setImplementacion(rs.getString("empr.empr_implementacion"));
                datosEmpresa.setUserSiigo(rs.getString("empr_userSiigo"));
                datosEmpresa.setPasswordSiigo(rs.getString("empr_passwordSiigo"));
                datosEmpresa.setEstado(rs.getString("empr_estado"));
                datosEmpresa.setCargueInicial(rs.getString("empr_cargueinicial"));
                datosEmpresa.setIdGerente(rs.getString("gere.usua_id"));
                datosEmpresa.setIdContadorAsignado(rs.getString("conas.usua_id"));
                datosEmpresa.setImplementadoSiigo(rs.getString("empr.empr_implementadosiigo"));
                datosEmpresa.setEstadoTareasEmpresa(rs.getString("estadoEmpresa"));
                datosEmpresa.setCorreoContador(rs.getString("conas.usua_correo"));
                datosEmpresa.setCorreoGerente(rs.getString("gere.usua_correo"));
                datosEmpresa.setNombreContador(rs.getString("nombreContador"));
                datosEmpresa.setNombreGerente(rs.getString("nombreGerente"));
                datosEmpresa.setCelularContador(rs.getString("conas.usua_celular"));
                datosEmpresa.setCelularGerente(rs.getString("gere.usua_celular"));
                datosEmpresa.setEstadoAnterior(rs.getString("empr.empr_estadoanterior"));
                datosEmpresa.setMunicipio(rs.getString("municipio.muni_nombre"));
                datosEmpresa.setRegistradoPor(rs.getString("empr.empr_registradopor"));
            }
            rs.close();
            ps.close();
            ps = null;
            rs = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return datosEmpresa;
    }

    public UsuarioDTO getUserByCargoEmpresa(String cargoEmpresa, String idEmpresa) {
        PreparedStatement ps = null;

        UsuarioDTO datos = null;
        StringBuilder cadSQL = null;
        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT conta.usua_id ,conta.empr_id,conta.caem_id  ");
            cadSQL.append(" FROM contador_empresa conta ");
            cadSQL.append(" WHERE conta.caem_id =? AND conta.empr_id =? ");
            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, cargoEmpresa, ps);
            AsignaAtributoStatement.setString(2, idEmpresa, ps);
            rs = ps.executeQuery();
            while (rs.next()) {
                datos = new UsuarioDTO();
                datos.setId(rs.getString("conta.usua_id"));
                datos.setIdEmpresa(rs.getString("conta.empr_id"));
                datos.setIdCargoEmpresa(rs.getString("conta.caem_id"));

            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return datos;
    }

    public UsuarioDTO getUserByCargoEmpresaAndContador(String cargoEmpresa, EmpresaDTO empresa) {
        PreparedStatement ps = null;

        UsuarioDTO datos = null;
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" select coem.usua_id,coem.usua_id  ");
            cadSQL.append(" from contador_empresa coem ");
            cadSQL.append(" inner join usuario usua on usua.usua_id = coem.usua_id ");
            cadSQL.append(" inner join contador_juridico coju on coju.coju_id = usua.coju_id ");
            cadSQL.append(" where usua.caem_id =? and coem.empr_id =? and coju.coju_id=usua.coju_id ");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, cargoEmpresa, ps);
            AsignaAtributoStatement.setString(2, empresa.getId(), ps);
            //AsignaAtributoStatement.setString(3, empresa.getIdContador(), ps);

            rs = ps.executeQuery();
            while (rs.next()) {
                datos = new UsuarioDTO();
                datos.setId(rs.getString("coem.usua_id"));
                datos.setIdEmpresa(rs.getString("coem.usua_id"));
            }
            ps.close();
            ps = null;
        } catch (Exception e) {

            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return datos;
    }

    //<editor-fold defaultstate="collapsed" desc="Bloque LogActividadesDAO">
    public boolean obtenerLogPorIdEjecucion(String idUsuario, String idEjecucion) {
        PreparedStatement ps = null;

        ContadorDTO contador = null;
        StringBuilder cadSQL = null;
        boolean existe = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT usua_iddevuelve , tilo_id , ejta_id ");
            cadSQL.append(" from log_actividades ");
            cadSQL.append(" WHERE usua_iddevuelve=? and tilo_id=? and ejta_id=? ");

            ps = conn.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, idUsuario, ps);
            AsignaAtributoStatement.setString(2, "3", ps);
            AsignaAtributoStatement.setString(3, idEjecucion, ps);

            rs = ps.executeQuery();
            while (rs.next()) {
                existe = true;
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return existe;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return existe;
            }
        }
        return existe;
    }

    public boolean registrarLogActividad(LogActividadesDTO datos, String estado, String observacion) throws SQLException {
        PreparedStatement ps = null;

        int nRows = 0;
        StringBuilder cadSQL = null;
        boolean registro = false;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO log_actividades(usua_iddevuelve ,usua_iddevolvieron, tilo_id ,load_observacion, ejta_id,empr_id,cotu_id,alte_id,cota_id) ");
            cadSQL.append(" VALUES(?,?,?,?,?,?,?,?,?) ");

            ps = conn.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);

            AsignaAtributoStatement.setString(1, datos.getIdUsuarioDevuelve(), ps);
            AsignaAtributoStatement.setString(2, datos.getIdUsuarioDevolvieron(), ps);
            AsignaAtributoStatement.setString(3, estado, ps);
            AsignaAtributoStatement.setString(4, observacion, ps);
            AsignaAtributoStatement.setString(5, datos.getIdEjecucion(), ps);
            AsignaAtributoStatement.setString(6, datos.getIdEmpresa(), ps);
            AsignaAtributoStatement.setString(7, datos.getIdContrato(), ps);
            AsignaAtributoStatement.setString(8, datos.getAlertaTemprana(), ps);
            AsignaAtributoStatement.setString(9, datos.getIdComentario(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    registro = true;
                    datos.setIdLogActividad(rs.getString(1));
                }

                rs.close();
                rs = null;
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return registro;
            }
        }
        return registro;
    }

    public boolean cambiarEstadoAlertaNoLeido(String idUsuario, String ejtaId, String estado, String tipoLog) {
        PreparedStatement ps = null;
        StringBuilder cadSQL = null;
        int nRows;
        boolean updated = false;
        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE log_actividades loac   ");
            cadSQL.append(" inner join ejecucion_tarea ejta on ejta.ejta_id = loac.ejta_id ");
            cadSQL.append(" SET loac.loac_listado=? ");
            cadSQL.append(" WHERE loac.usua_iddevuelve = ? and loac.tilo_id=? and loac.ejta_id = ? and ejta.ejta_estado");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, estado, ps);
            AsignaAtributoStatement.setString(2, idUsuario, ps);
            AsignaAtributoStatement.setString(3, tipoLog, ps);
            AsignaAtributoStatement.setString(4, ejtaId, ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                updated = true;
                //System.out.println("Se cambio estado de alertas a leido");
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return updated;
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Bloque EjecucionTareaDAO">
    public ActividadPreDTO proximaEjecucionAjena(EjecucionTareaDTO tarea) {
        ActividadPreDTO ejecucion = null;
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT ejta_id,usua_id FROM ejecucion_tarea ");
            cadSQL.append(" WHERE ejta_codejecucion = ? AND taem_id = ? AND ejta_id = (?+1) AND usua_id <> ? ");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, tarea.getCodEjcucion(), ps);
            AsignaAtributoStatement.setString(2, tarea.getIdTareaEmpresa(), ps);
            AsignaAtributoStatement.setString(3, tarea.getId(), ps);
            AsignaAtributoStatement.setString(4, tarea.getIdUsuario(), ps);

            rs = ps.executeQuery();
            while (rs.next()) {
                ejecucion = new ActividadPreDTO();
                ejecucion.setId(rs.getString("ejta_id"));
                ejecucion.setIdUsuario(rs.getString("usua_id"));
            }
            ps.close();
            ps = null;
            rs.close();
            rs = null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return ejecucion;
    }

    public String ultimaEjecucion(String idEmpresa, String idTareaEmpresa) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        String ultimaEjecucion = Generales.EMPTYSTRING;
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" select IF( MAX(CAST(ejta.ejta_codejecucion AS UNSIGNED)) > 0  ,MAX(CAST(ejta.ejta_codejecucion AS UNSIGNED)) + 1,1) consecutivo from ejecucion_tarea ejta ");
            cadSQL.append(" inner join tarea_empresa taem on taem.taem_id =  ejta.taem_id ");
            cadSQL.append(" inner join empresa empr on empr.empr_id = taem.empr_id ");
            cadSQL.append(" left JOIN proceso_empresa pr_empr ON pr_empr.empr_id = taem.empr_id ");
            cadSQL.append(" where empr.empr_id = ?  and if(pr_empr.prem_estado is not null, pr_empr.prem_estado = '1',pr_empr.prem_estado is null )  and taem.taem_id=? ");
            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idEmpresa, ps);
            AsignaAtributoStatement.setString(2, idTareaEmpresa, ps);

            rs = ps.executeQuery();
            while (rs.next()) {
                ultimaEjecucion = rs.getString("consecutivo");
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return ultimaEjecucion;

    }

    public boolean registrarEjecucionTarea(ActividadPredeterminadaDTO datos, String ultimaEjecucion, String idTareaEmpresa, String registradoPor, String ambiente) throws SQLException {
        int nRows = 0;
        StringBuilder cadSQL = null;
        EmpresaDTO respuesta = null;
        boolean registro = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO ejecucion_tarea (ejta_codejecucion,taem_id,ejta_fechainicial,ejta_fechafinal, ");
            cadSQL.append(" ejta_diashabiles,ejta_diasprorroga,ejta_orden,ejta_descripcion,acpr_id,usua_id,ejta_estado,ejta_registradopor,ejta_habil,caem_id ) ");
            cadSQL.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

            ps = conn.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);

            AsignaAtributoStatement.setString(1, ultimaEjecucion, ps);
            AsignaAtributoStatement.setString(2, idTareaEmpresa, ps);
            AsignaAtributoStatement.setString(3, datos.getFechaInicial(), ps);
            AsignaAtributoStatement.setString(4, datos.getFechaFinal(), ps);
            AsignaAtributoStatement.setString(5, datos.getDiasDuracion(), ps);
            AsignaAtributoStatement.setString(6, datos.getDiasProrroga(), ps);
            AsignaAtributoStatement.setString(7, datos.getOrdenActividad(), ps);
            AsignaAtributoStatement.setString(8, datos.getDescripcion(), ps);
            AsignaAtributoStatement.setString(9, datos.getActividad(), ps);
            AsignaAtributoStatement.setString(10, datos.getIdUsuarioPorCargo(), ps);

            if (datos.getEstado() != null && !datos.getEstado().equals("")) {
                AsignaAtributoStatement.setString(11, datos.getEstado(), ps);
            } else {
                AsignaAtributoStatement.setString(11, "0", ps);
            }
            AsignaAtributoStatement.setString(12, registradoPor, ps);
            if (ambiente.equals("produccion")) {
                AsignaAtributoStatement.setString(13, "0", ps);
            } else if (ambiente.equals("prueba")) {
                AsignaAtributoStatement.setString(13, "1", ps);
            }
            AsignaAtributoStatement.setString(14, datos.getCargoActividad(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    respuesta = new EmpresaDTO();
                    //respuesta.setId(rs.getString(1));
                    datos.setId(rs.getString(1));
                    registro = true;
                }

                rs.close();
                rs = null;
            }
        } catch (Exception se) {
            se.printStackTrace();
            //System.out.println("registro ERROR: " + datos.toStringJson());
            //System.out.println("ID tarea ERROR: " + idTareaEmpresa);
            se.printStackTrace();
            respuesta = new EmpresaDTO();
            respuesta.setId("");
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                System.out.println("==>>>>><");
                e.printStackTrace();
            }
        }
        return registro;
    }

    public TareaPredeterminadaDTO listarEjecucionPorIdTareaEmpresa(String idTareaEmpresa) throws SQLException {
        StringBuilder cadSQL = null;
        TareaPredeterminadaDTO datosEjecucion = null;

        try {
            cadSQL = new StringBuilder();

            cadSQL.append(" SELECT cale.catr_recurrencia,SUBSTRING_INDEX( cale.catr_fecha,\"-\",-1) as diames,cale.catr_diasantes,ejta_id, ejta_fechainicial,IFNULL(taem.cafe_id,'') AS cafeid, taem.tapr_id, CAST(ejta_codejecucion AS UNSIGNED) as codeje, taem_impuesto , taem_diahabilsiguiente from ejecucion_tarea ejta");
            cadSQL.append(" inner join tarea_empresa taem on taem.taem_id =  ejta.taem_id ");
            cadSQL.append(" inner join empresa empr on empr.empr_id =  taem.empr_id ");
            cadSQL.append(" LEFT JOIN calendario_tributario cale ON cale.tapr_id = taem.tapr_id  AND (cale.catr_ultimodigitonit = SUBSTRING(empr.empr_nit, -2, 2)  OR cale.catr_ultimodigitonit = SUBSTRING(empr.empr_nit, -1, 1) )");
            cadSQL.append("  WHERE taem.taem_id= ? AND ejta_habil = ?  ");
            cadSQL.append("   ORDER BY codeje DESC, ejta_fechainicial ASC  LIMIT 1 ");

            ps = conn.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);

            AsignaAtributoStatement.setString(1, idTareaEmpresa, ps);
            AsignaAtributoStatement.setString(2, "1", ps);
            rs = ps.executeQuery();
            while (rs.next()) {
                datosEjecucion = new TareaPredeterminadaDTO();
                datosEjecucion.setIdEjecucion(rs.getString("ejta_id"));
                datosEjecucion.setFechaInicialReal(rs.getString("ejta_fechainicial"));
                datosEjecucion.setId(rs.getString("taem.tapr_id"));
                datosEjecucion.setCodigoEjecucion(rs.getString("codeje"));
                datosEjecucion.setImpuesto(rs.getString("taem_impuesto"));
                datosEjecucion.setDiaHabilSiguiente(rs.getString("taem_diahabilsiguiente"));
                datosEjecucion.setIdTareaEmpresa(idTareaEmpresa);
                datosEjecucion.setCalendarioFecha(rs.getString("cafeid"));
                datosEjecucion.setRecurrente(rs.getString("cale.catr_recurrencia"));
                datosEjecucion.setDiasAntes(rs.getString("cale.catr_diasantes"));
                datosEjecucion.setDiasMes(rs.getString("diames"));
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return datosEjecucion;
            }
        }
        return datosEjecucion;
    }

    public EjecucionProximaDTO validarEjecucionProximaPorIdTareaEmpresa(String idTareaEmpresa) {
        EjecucionProximaDTO ejecucion = null;
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT ejpr_id FROM ejecucion_proxima ");
            cadSQL.append(" WHERE taem_id = ?  ");
            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idTareaEmpresa, ps);

            rs = ps.executeQuery();

            while (rs.next()) {
                ejecucion = new EjecucionProximaDTO();
                ejecucion.setIdEjecucionProxima(rs.getString("ejpr_id"));
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return ejecucion;
    }

    public boolean actualizarEjecucionTareaProxima(EjecucionProximaDTO datos, String ultimoCodEjecucion) throws SQLException {
        int nRows = 0;
        StringBuilder cadSQL = null;
        EmpresaDTO respuesta = null;
        boolean registro = false;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" UPDATE ejecucion_proxima SET ejpr_fechainicial = ?, ejta_codejecucion = ?, cafe_id = ? WHERE taem_id = ?  ");

            ps = conn.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);

            AsignaAtributoStatement.setString(1, datos.getFechaInicialProxima(), ps);
            AsignaAtributoStatement.setString(2, ultimoCodEjecucion, ps);
            AsignaAtributoStatement.setString(3, datos.getCalendarioFecha(), ps);
            AsignaAtributoStatement.setString(4, datos.getIdTareaEmpresa(), ps);

            nRows = ps.executeUpdate();
            if (nRows >= 0) {
                registro = true;
            }
        } catch (Exception se) {
            se.printStackTrace();
            //System.out.println("registro ERROR: " + datos.toStringJson());
            //System.out.println("ID tarea ERROR: " + idTareaEmpresa);
            respuesta = new EmpresaDTO();
            respuesta.setId("");
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                System.out.println("==>>>>><");
                e.printStackTrace();
            }
        }
        return registro;
    }

    public boolean registrarEjecucionTareaProxima(EjecucionProximaDTO datos) throws SQLException {
        int nRows = 0;
        StringBuilder cadSQL = null;
        EmpresaDTO respuesta = null;
        boolean registro = false;

        try {

            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO ejecucion_proxima (ejpr_fechainicial, ejta_codejecucion, taem_id, cafe_id)");
            cadSQL.append("  VALUES (?, ?, ?, ? );");

            ps = conn.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);

            AsignaAtributoStatement.setString(1, datos.getFechaInicialProxima(), ps);
            AsignaAtributoStatement.setString(2, datos.getCodigoEjecucionProxima(), ps);
            AsignaAtributoStatement.setString(3, datos.getIdTareaEmpresa(), ps);
            AsignaAtributoStatement.setString(4, datos.getCalendarioFecha(), ps);

            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    respuesta = new EmpresaDTO();
                    datos.setIdEjecucionProxima(rs.getString(1));
                    registro = true;
                }

                rs.close();
                rs = null;
            }
        } catch (Exception se) {
            se.printStackTrace();
            respuesta = new EmpresaDTO();
            respuesta.setId("");
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                System.out.println("==>>>>><");
                e.printStackTrace();
            }
        }
        return registro;
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Bloque EmpresaDAO">
    public String consultarIdContadorEncargado(String idEmpresa) {

        String datos = "";
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT usua_id FROM contador_empresa");
            cadSQL.append(" WHERE empr_id = ? AND caem_id = ? ");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idEmpresa, ps);
            AsignaAtributoStatement.setString(2, "12", ps);

            rs = ps.executeQuery();
            while (rs.next()) {
                datos = rs.getString("usua_id");
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return datos;
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Bloque CargosEmpresaDAO">
    public UsuarioDTO consultarIdUsuarioResponsableAlertasPorIdEmpresa(String idEmpresa) {

        UsuarioDTO responsable = null;
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT coem_id,usua_id,caem_id FROM contador_empresa WHERE empr_id = ? AND coem_alertas ='1' ");
            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idEmpresa, ps);

            rs = ps.executeQuery();

            while (rs.next()) {
                responsable = new UsuarioDTO();
                responsable.setId(rs.getString("usua_id"));
                responsable.setIdCargoEmpresa(rs.getString("caem_id"));
                responsable.setCargoEmpresa(rs.getString("coem_id"));
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();

                return null;
            }
        }
        return responsable;
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Bloque DiasDAO">
    public ArrayList<String> listarDiasFestivos() {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<String> listado = null;
        StringBuilder cadSQL = null;
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT dias_fechas ");
            cadSQL.append(" FROM dias  ");

            ps = conn.prepareStatement(cadSQL.toString());

            rs = ps.executeQuery();

            listado = new ArrayList();
            while (rs.next()) {
                listado.add(rs.getString("dias_fechas"));
            }

            ps.close();
            ps = null;
            rs.close();
            rs = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
                if (listado != null && listado.isEmpty()) {
                    listado = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return listado;
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Bloque TareaPredeterminadaDAO">
    public ArrayList<CheckListDTO> obtenerCheckTareaEmpresa(String idTareaEmpresaIdActividad) {

        StringBuilder cadSQL = null;
        ArrayList<CheckListDTO> listado = new ArrayList<>();
        CheckListDTO check = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT chte_id,chte_orden,chte_descripcion,chte_ayuda,chte_estado ");
            cadSQL.append(" FROM checklist_tarea_empresa ");
            cadSQL.append(" WHERE taem_idactividad = ? ");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, idTareaEmpresaIdActividad, ps);

            rs = ps.executeQuery();
            while (rs.next()) {
                check = new CheckListDTO();
                check.setIdCheck(rs.getString("chte_id"));
                check.setDescripcion(rs.getString("chte_descripcion"));
                check.setAyuda(rs.getString("chte_ayuda"));
                check.setOrden(rs.getString("chte_orden"));
                check.setEstado(rs.getString("chte_estado"));

                listado.add(check);

            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        return listado;

    }

    public String getIdTpAp(String tareaEmpresa, String actividad_id) {

        StringBuilder cadSQL = null;
        String respuesta = "";
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT tpap.tpap_id ");
            cadSQL.append(" FROM tareapre_actividadpre tpap ");
            cadSQL.append(" inner join tarea_empresa taem on taem.tapr_id = tpap.tapr_id ");
            cadSQL.append(" where taem.taem_id = ? and tpap.acpr_id=? ");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, tareaEmpresa, ps);
            AsignaAtributoStatement.setString(2, actividad_id, ps);

            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString("tpap_id");
            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        return respuesta;

    }

    public ArrayList<CheckListDTO> obtener(String tareaPreActividadPre) {

        StringBuilder cadSQL = null;
        ArrayList<CheckListDTO> listado = new ArrayList<>();
        CheckListDTO check = null;

        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" SELECT chec_id,chec_orden,chec_descripcion,chec_ayuda ");
            cadSQL.append(" FROM checklist ");
            cadSQL.append(" WHERE tpap_id = ? ");

            ps = conn.prepareStatement(cadSQL.toString());
            AsignaAtributoStatement.setString(1, tareaPreActividadPre, ps);

            rs = ps.executeQuery();
            while (rs.next()) {
                check = new CheckListDTO();
                check.setIdCheck(rs.getString("chec_id"));
                check.setDescripcion(rs.getString("chec_descripcion"));
                check.setAyuda(rs.getString("chec_ayuda"));
                check.setOrden(rs.getString("chec_orden"));

                listado.add(check);

            }
            ps.close();
            ps = null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        return listado;

    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Bloque ContanidoChecklistDAO">
    public boolean registrarChecklistActividad(CheckListDTO checklist, String eje) {
        StringBuilder cadSQL = null;
        RespuestaVistaDTO respuesta = new RespuestaVistaDTO();
        int nRows = 0;
        //System.out.println(checklist.toStringJson() + "<----Contenido");
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO checklist_actividadempresa(clac_orden,clac_descripcion,ejta_id)");
            cadSQL.append(" VALUES (?, ?, ? );");
            ps = conn.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);

            AsignaAtributoStatement.setString(1, checklist.getOrden(), ps);
            AsignaAtributoStatement.setString(2, checklist.getDescripcion(), ps);
            AsignaAtributoStatement.setString(3, eje, ps);
            // AsignaAtributoStatement.setString(4, checklist.getAyuda(), ps);

            nRows = ps.executeUpdate();

            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    respuesta = new RespuestaVistaDTO();
                    respuesta.setRegistro(true);
                }

                rs.close();
                rs = null;
            }

        } catch (SQLException se) {
            se.printStackTrace();
            respuesta = new RespuestaVistaDTO();
            return respuesta.isRegistro();
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return respuesta.isRegistro();
            }
        }
        return respuesta.isRegistro();
    }

//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Bloque CheclistTareaEMpresaDAO">
    public String obtenerOrdenNuevoCheck( String idtareaActividad) throws SQLException {
        String orden = "1";
        StringBuilder cadSQL = null;

        try {
            cadSQL = new StringBuilder();

            cadSQL.append("   select IF(chte_orden is not NULL,sum(chte_orden + 1), 1) as ordenNuevo ");
            cadSQL.append("   from (SELECT chte_orden  FROM checklist_tarea_empresa chec ");
            cadSQL.append("   WHERE chec.taem_idactividad = ? ORDER BY chte_orden DESC LIMIT 1) as subt ");
            cadSQL.append("   group by subt.chte_orden;");

            ps = conn.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);
            AsignaAtributoStatement.setString(1, idtareaActividad, ps);

            rs = ps.executeQuery();
            while (rs.next()) {
                orden = rs.getString("ordenNuevo");
            }
            ps.close();
            ps = null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return orden;
    }

    public RespuestaVistaDTO registrarCheckListTareaEmpresa( CheckListDTO checklist, String idTareaEmpresa, String idActividad) {
        StringBuilder cadSQL = null;
        RespuestaVistaDTO respuesta = new RespuestaVistaDTO();
        int nRows = 0;
        //System.out.println(checklist.toStringJson() + "<----Contenido");
        try {
            cadSQL = new StringBuilder();
            cadSQL.append(" INSERT INTO checklist_tarea_empresa(chte_orden,chte_descripcion,taem_idactividad,chte_ayuda)");
            cadSQL.append(" VALUES (?, ?, ?, ?) ");
            ps = conn.prepareStatement(cadSQL.toString(), Statement.RETURN_GENERATED_KEYS);

            AsignaAtributoStatement.setString(1, checklist.getOrden(), ps);
            AsignaAtributoStatement.setString(2, checklist.getDescripcion(), ps);
            AsignaAtributoStatement.setString(3, idTareaEmpresa + "-" + idActividad, ps);
            AsignaAtributoStatement.setString(4, checklist.getAyuda(), ps);
            nRows = ps.executeUpdate();
            if (nRows > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    respuesta.setRegistro(true);
                }

                rs.close();
                rs = null;
            } else {
                respuesta = new RespuestaVistaDTO();
                respuesta.setRegistro(false);
            }

        } catch (SQLException se) {
            se.printStackTrace();
            respuesta.getMensajes().add(se.getMessage());
            System.out.println("errorrr>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><");
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                    ps = null;
                }

            } catch (SQLException e) {
                System.out.println("ERROR:: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><");
                e.printStackTrace();
            }
        }
        return respuesta;

    }

//</editor-fold>
}
